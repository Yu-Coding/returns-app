# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_23_161204) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "customers", force: :cascade do |t|
    t.string "customer_id"
    t.string "stripe_id"
    t.string "order_id"
    t.string "name"
    t.string "country"
    t.string "state"
    t.string "city"
    t.string "zip_code"
    t.string "address1"
    t.string "address2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "line_item_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "order_id"
    t.string "new_order_id"
    t.string "status"
    t.integer "reimbursement_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "order_id"
    t.string "order_number"
    t.string "line_item_id"
    t.string "product_id"
    t.string "variant_id"
    t.string "sku"
    t.integer "quantity"
    t.string "exchange_product_id"
    t.string "exchange_variant_id"
    t.integer "reimbursement_type_id"
    t.integer "return_reason_id"
    t.integer "return_method_id"
    t.integer "return_container_id"
    t.string "received_quantity", default: "0"
    t.string "return_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "processed", default: false
  end

  create_table "reimbursement_types", force: :cascade do |t|
    t.string "return_type"
    t.string "normal_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "return_containers", force: :cascade do |t|
    t.string "container"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "return_methods", force: :cascade do |t|
    t.string "short_description"
    t.string "description"
    t.integer "reimbursement_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "return_reasons", force: :cascade do |t|
    t.string "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shops", force: :cascade do |t|
    t.string "shopify_domain", null: false
    t.string "shopify_token", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shopify_domain"], name: "index_shops_on_shopify_domain", unique: true
  end

end
