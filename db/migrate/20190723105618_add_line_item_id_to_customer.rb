class AddLineItemIdToCustomer < ActiveRecord::Migration[5.2]
  def change
    add_column :customers, :line_item_id, :integer, :limit => 8
  end
end
