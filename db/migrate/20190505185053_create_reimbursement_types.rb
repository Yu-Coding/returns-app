class CreateReimbursementTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :reimbursement_types do |t|
      t.string :return_type
      t.string :normal_type

      t.timestamps
    end
  end
end
