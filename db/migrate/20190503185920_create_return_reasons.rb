class CreateReturnReasons < ActiveRecord::Migration[5.2]
  def change
    create_table :return_reasons do |t|
      t.string :reason

      t.timestamps
    end
  end
end
