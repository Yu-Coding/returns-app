class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :order_id
      t.string :new_order_id
      t.string :status
      t.integer :reimbursement_type_id

      t.timestamps
    end
  end
end
