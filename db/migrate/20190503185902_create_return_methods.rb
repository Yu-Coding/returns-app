class CreateReturnMethods < ActiveRecord::Migration[5.2]
  def change
    create_table :return_methods do |t|
      t.string :short_description
      t.string :description
      t.integer :reimbursement_type_id
      
      t.timestamps
    end
  end
end
