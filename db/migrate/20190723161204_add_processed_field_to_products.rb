class AddProcessedFieldToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :processed, :boolean, :default => false
  end
end
