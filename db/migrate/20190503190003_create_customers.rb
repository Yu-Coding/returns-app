class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :customer_id
      t.string :stripe_id
      t.string :order_id
      t.string :name
      t.string :country
      t.string :state
      t.string :city
      t.string :zip_code
      t.string :address1
      t.string :address2

      t.timestamps
    end
  end
end
