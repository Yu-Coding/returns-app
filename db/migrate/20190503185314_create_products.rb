class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :order_id
      t.string :order_number
      t.string :line_item_id
      t.string :product_id
      t.string :variant_id
      t.string :sku
      t.integer :quantity
      t.string :exchange_product_id
      t.string :exchange_variant_id
      t.integer :reimbursement_type_id
      t.integer :return_reason_id
      t.integer :return_method_id
      t.integer :return_container_id
      t.string :received_quantity, default: 0
      t.string :return_id

      t.timestamps
    end
  end
end
