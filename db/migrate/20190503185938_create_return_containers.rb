class CreateReturnContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :return_containers do |t|
      t.string :container

      t.timestamps
    end
  end
end
