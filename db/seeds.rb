# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


    # Shop.create(shopify_domain: 'mack-weldon-qa.myshopify.com', shopify_token: '5468c06001021cc41931bb6617501276')
    # Shop.create(shopify_domain: 'mack-weldon.myshopify.com', shopify_token: '214e28412a012568ce80b74835279903')

    reasons = [
        'item too large',
        'item too small',
        'quality not as expected',
        'not the right style',
        'no longer wanted or needed',
        'ordered by mistake',
        'incorrect item received',
    ]
    reasons.each do |reason|
        ReturnReason.create(reason: reason)
    end

    reimbursement_types = [
        ['refund', 'TOG'],
        ['exchange', 'TOG'],
        ['refund', 'STANDARD'],
        ['exchange', 'STANDARD'],
    ]
    reimbursement_types.each do |type|
        ReimbursementType.create(return_type: type[0], normal_type: type[1])
    end

    containers = ['envelope', 'box']
    containers.each do |container|
        ReturnContainer.create(container: container)
    end

    methods = [
        ['refund as store credit to my<br>account', 'refund as store credit', 1],
        ['refund to my original form of<br>payment', 'refund my original payment', 1],
        ['refund as store credit to my<br>account', 'return & refund as store credit', 3],
        ['refund to my original form of<br>payment', 'return & refund my original payment ', 3]
    ]
    methods.each do |method|
        ReturnMethod.create(short_description: method[0], description: method[1], reimbursement_type_id: method[2])
    end

    # 18-Hour Jersey - Boxer Briefs
    # variants = [
    #     ["Blue Night", "S"],
    #     ["Blue Yonder", "S"],
    #     ["Bluecrest", "S"],
    #     ["Bright White", "S"],
    #     ["Calder Blue", "S"],
    #     ["Charcoal Heather", "S"],
    #     ["Cloud Burst", "S"],
    #     ["Crimson", "S"],
    #     ["Crimson / Gargoyle", "S"],
    #     ["Crimson Bison Check", "S"],
    #     ["Crimson Crossbow", "S"],
    #     ["Crimson Heather", "S"],
    #     ["Dark Forest", "L"],
    #     ["Dark Grey Tartan", "S"],
    #     ["Dark Rum", "S"],
    #     ["Grey Heather", "S"],
    #     ["Husk", "S"],
    #     ["Monument / Taxi", "S"],
    #     ["Monument Grey / Asphalt / Weldon Blue", "S"],
    #     ["Monument Origami", "S"],
    #     ["Nine Iron", "S"],
    #     ["Nine Iron Bison Check", "S"],
    #     ["No Fear Red", "S"],
    #     ["Purple Fog / True Navy", "S"],
    #     ["Russet Dots", "L"],
    #     ["Strong Blue / Firestarter", "S"],
    #     ["Total Eclipse", "S"],
    #     ["Total Eclipse Dots", "S"],
    #     ["Total Eclipse Draper Diamond", "S"],
    #     ["Total Eclipse Origami", "S"],
    #     ["True Black", "S"],
    #     ["True Black / Monument", "S"],
    #     ["True Black / Total Eclipse", "S"],
    #     ["True Black Draper Diamond", "S"],
    #     ["Weldon Blue", "S"],
    # ]

    # variants.each do |variant|
    #     TogProduct.create(
    #         :product_id => '',
    #         :title => "18-Hour Jersey - Boxer Briefs",
    #         :product_type => 'Underwear',
    #         :tags => "Boxer Briefs, TOG-18-Hour",
    #         :color => variant[0],
    #         :size => variant[1]
    #     )
    # end


    # # 18-Hour Jersey - Briefs
    # variants = [
    #     ["Aquifer / Denim", "S"],
    #     ["Blue Night", "S"],
    #     ["Blue Yonder", "S"],
    #     ["Bluecrest", "S"],
    #     ["Bright White", "S"],
    #     ["Cactus / Campus Green", "S"],
    #     ["Calder Blue", "S"],
    #     ["Cloud Burst", "S"],
    #     ["Crimson", "S"],
    #     ["Crimson / Gargoyle", "S"],
    #     ["Crimson Crossbow", "S"],
    #     ["Dark Forest", "S"],
    #     ["Dark Grey Tartan", "S"],
    #     ["Ember", "S"],
    #     ["Gold Coast", "S"],
    #     ["Monument / Taxi", "S"],
    #     ["Monument Grey / Asphalt / Weldon Blue", "S"],
    #     ["Nine Iron Bison Check", "S"],
    #     ["No Fear Red", "S"],
    #     ["Purple Fog / True Navy", "S"],
    #     ["Rescue Orange", "S"],
    #     ["Rescue Orange / Ember", "S"],
    #     ["Russet Dots", "S"],
    #     ["Stonewash", "S"],
    #     ["Stonewash / Bright White", "S"],
    #     ["Strong Blue / Firestarter", "S"],
    #     ["Total Eclipse", "S"],
    #     ["Total Eclipse Origami", "S"],
    #     ["Total Eclipse Tartan", "S"],
    #     ["True Black", "S"],
    #     ["True Black / Monument", "S"],
    #     ["True Black / Total Eclipse", "S"],
    #     ["Weldon Blue", "S"],
    # ]

    # variants.each do |variant|
    #     TogProduct.create(
    #         :product_id => '',
    #         :title => "18-Hour Jersey - Briefs",
    #         :product_type => 'Underwear',
    #         :tags => "Briefs, TOG-18-Hour",
    #         :color => variant[0],
    #         :size => variant[1]
    #     )
    # end


    # # 18-Hour Jersey - Trunks
    # variants = [
    #     ["Blue Night", "S"],
    #     ["Blue Yonder", "S"],
    #     ["Bluecrest", "S"],
    #     ["Bright White", "S"],
    #     ["Calder Blue", "S"],
    #     ["Charcoal Heather", "S"],
    #     ["Cloud Burst", "S"],
    #     ["Crimson / Gargoyle", "S"],
    #     ["Crimson Bison Check", "S"],
    #     ["Crimson Crossbow", "S"],
    #     ["Crimson Heather", "S"],
    #     ["Dark Forest", "S"],
    #     ["Dark Grey Tartan", "S"],
    #     ["Dark Rum", "S"],
    #     ["Grey Heather", "S"],
    #     ["Monument / Taxi", "S"],
    #     ["Monument Grey / Asphalt / Weldon Blue", "S"],
    #     ["Nine Iron", "S"],
    #     ["No Fear Red", "S"],
    #     ["Purple Fog / True Navy", "S"],
    #     ["Russet", "S"],
    #     ["Russet Dots", "S"],
    #     ["Stonewash / Bright White", "S"],
    #     ["Strong Blue / Firestarter", "S"],
    #     ["Total Eclipse Origami", "S"],
    #     ["Total Eclipse Tartan", "S"],
    #     ["True Black", "S"],
    #     ["True Black / Monument", "S"],
    #     ["True Black / Total Eclipse", "S"],
    #     ["True Black Draper Diamond", "S"],
    # ]

    # variants.each do |variant|
    #     TogProduct.create(
    #         :product_id => '',
    #         :title => "18-Hour Jersey - Trunks",
    #         :product_type => 'Underwear',
    #         :tags => "TOG-18-Hour, Trunks",
    #         :color => variant[0],
    #         :size => variant[1]
    #     )
    # end


    # products = [
    #     ["18-Hour Mesh - Boxer Briefs", "Underwear", "Boxer Briefs, TOG-18-Hour", "Total Eclipse / Valencia", "S"],
    #     ["AIRKNITx - Ankle Sock", "Socks", "Low, TOG-One Size Sock", "Grey Heather", "One Size"],
    #     ["AIRKNITx - Boxer Briefs", "Underwear", "Boxer Briefs, TOG-AIRKNITx", "Grey Heather", "S"],
    #     ["AIRKNITx - Briefs", "Underwear", "Briefs, TOG-AIRKNITx", "Indigo Heather", "S"],
    #     ["AIRKNITx - Crew Sock", "Socks", "High, TOG-One Size Sock", "Total Eclipse Blue", "One Size"],
    #     ["AIRKNITx - High Ankle Sock", "Socks", "Low, TOG-One Size Sock", "True Black / Taxi", "One Size"],
    #     ["AIRKNITx - Trunk", "Trunk", "TOG-AIRKNITx", "Indigo Heather", "S"],
    #     ["AIRKNITx 8\" - Boxer Briefs", "Underwear", "Extended Boxer Briefs, TOG-AIRKNITx", "Charcoal Heather", "S"],
    #     ["Boot - Extended Crew Sock", "Socks", "High, TOG-One Size Sock", "Alloy", "One Size"],
    #     ["Cashmere - Extended Crew Sock", "Socks", "High, TOG-One Size Sock", "Charcoal / Blue / Black", "One Size"],
    #     ["Everyday - Extended Crew Sock", "Socks", "High, TOG-One Size Sock", "Grey / Weldon Blue", "One Size"],
    #     ["Silver - Boxer Briefs", "Underwear", "Boxer Briefs, TOG-Silver", "Stealth Grey", "S"],
    #     ["Silver - Extended Crew Dress Sock", "Socks", "High, TOG-One Size Sock", "True Black", "One Size"],
    #     ["Silver - Trunks", "Underwear", "TOG-Silver, Trunks", "True Navy", "S"],
    #     ["Woven - Boxers", "Underwear", "Boxers, TOG-Woven", "Indigo Dot", "S"]
    # ]

    # products.each do |product|
    #     TogProduct.create(
    #         :product_id => '',
    #         :title => product[0],
    #         :product_type => product[1],
    #         :tags => product[2],
    #         :color => product[3],
    #         :size => product[4]
    #     )
    # end

    # colors = [
    #     ["Asphalt", "", "#444141", "", "color"],
    #     ["Bluecrest", "", "#acd1e3", "", "color"],
    #     ["Blue Haze", "", "#8aa1c7", "", "color"],
    #     ["Blue Haze Jigsaw", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_Jigsaw_Blue_Haze_True_Navy_53aacf51-9252-41dd-91bb-030ba3adb159_46x.jpg?v=1559765772", "#000", "", "image"],
    #     ["Blue Night", "", "#252850", "", "color"],
    #     ["Blue Yonder", "", "#97aacb", "", "color"],
    #     ["Bright White", "", "#f8f8ff", "", "color"],
    #     ["Camp Green Prairie Vine", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_PrarieVine_CampGreen_CovertGreen_46x.jpg?v=1559766641", "#000", "", "image"],
    #     ["Charcoal Heather", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/charcoal_hthr_46x.jpeg?v=1560188922", "#cdcbcc", "", "image"],
    #     ["Cloud Burst", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/NoShowGrey_Heather_46x.jpg?v=1559764930", "#353e4f", "", "image"],
    #     ["Crimson", "", "#ae0908", "", "color"],
    #     ["Crimson Bison Check", "", "#000000", "", "color"],
    #     ["Crimson Crossbow", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/Crossbow_Crimson_1_46x.jpeg?v=1559766428", "#000", "", "image"],
    #     ["Crimson Heather", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/Crimson_Heather_46x.png?v=1559765170", "#000", "", "image"],
    #     ["Crimson / Gargoyle", "", "#990000", "#373735", "color"],
    #     ["Dark Forest", "", "#2a5543", "", "color"],
    #     ["Dark Grey Tartan", "", "#a9a9a9", "", "color"],
    #     ["Dark Marl Grey", "", "#808080", "", "color"],
    #     ["Dark Rum", "", "#5b3132", "", "color"],
    #     ["Dean Blue / True Navy / Taxi", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_Weldon_United_Dean_Blue_True_Navy_Taxi_46x.jpg?v=1559765647", "#000", "", "image"],
    #     ["Desert Heather", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/deserty_heather_swatch_46x.jpeg?v=1560189639", "##ac94a8", "", "image"],
    #     ["Duffle Bag Green", "", "#526850", "", "color"],
    #     ["Firestarter", "", "#d9530a", "", "color"],
    #     ["Gold Coast", "", "#ffe7ae", "", "color"],
    #     ["Grey Heather", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/Grey_Heather_46x.jpeg?v=1559765874shopify:\/\/shop_images\/Grey_Heather.jpeg", "#c1c1c9", "", "image"],
    #     ["Grey Marl / Firestarter", "", "#808080", "f1ad4b", "color"],
    #     ["Harvest", "", "#ff8f5a", "", "color"],
    #     ["Husk", "", "#f6d28c", "", "color"],
    #     ["Indigo Heather", "", "#2e415c", "", "color"],
    #     ["Monument Grey / Asphalt / Weldon Blue", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_WeldonStripe_MonumentAsphalt_WeldonBlue_46x.jpg?v=1559766303", "#000", "", "image"],
    #     ["Monument Origami", "", "#a1c7b6", "", "color"],
    #     ["Monument / Taxi", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_LighthouseStripe_Monument_Taxi_c619b1ac-7325-4757-8f27-e356f965df6d_46x.jpg?v=1559765821", "#a1c7b6", "#fb9403", "image"],
    #     ["Navy Heather", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/Navy_heather_Swatch_46x.jpg?v=1559766077", "#000", "", "image"],
    #     ["Nine Iron", "", "#555555", "", "color"],
    #     ["Nine Iron Bison Check", "", "#434b4d", "", "color"],
    #     ["No Fear Red", "", "#fa373b", "", "color"],
    #     ["Purple Fog / True Navy", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_MackStripe_PurpleFog_Navy_46x.jpg?v=1559765195", "#5d4e64", "#003366", "image"],
    #     ["Red Clay", "", "#cc4831", "", "color"],
    #     ["Red Clay Marl", "", "#ad5049", "", "color"],
    #     ["Russet Dots", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/Russet_Dots_alternate_46x.png?v=1559765086", "#80461b", "", "image"],
    #     ["Sand Heather", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/sand_heather_46x.jpg?v=1560188640", "#000", "", "image"],
    #     ["Seamaster", "", "#3a5d63", "", "color"],
    #     ["Seamaster / Dean Blue", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_Lighthouse_Seamaster-DeanBlue_1_46x.jpg?v=1560181998", "#000", "", "image"],
    #     ["Sharkskin", "", "#586ca2", "", "color"],
    #     ["Smokey Mountain", "", "#74a4ab", "", "color"],
    #     ["Stonewash", "", "#9fa4c2", "", "color"],
    #     ["Storm Heather", "", "#7f8595", "", "color"],
    #     ["Strong Blue / Firestarter", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_LighthouseStripe_Strong_Blue_Firestarter_bb8a5288-400d-4d41-9df4-0bc405c9a935_46x.jpg?v=1559765810", "#0892d0", "#f1ad4b", "image"],
    #     ["Total Eclipse Blue", "", "#2c364a", "", "color"],
    #     ["Total Eclipse", "", "#2a323d", "", "color"],
    #     ["Total Eclipse Draper Diamond", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_DraperDiamond_TotalEclipse_30901cab-45f4-49cd-93b5-b86848350486_46x.jpg?v=1559765837", "#6a696f", "", "image"],
    #     ["Total Eclipse Dots", "", "#2c3540", "", "color"],
    #     ["Total Eclipse Origami", "", "#2c3540", "", "color"],
    #     ["True Black", "", "#000000", "", "color"],
    #     ["True Black Draper Diamond", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_DraperDiamond_TrueBlack_46x.jpg?v=1559764848", "#000", "", "image"],
    #     ["True Black / Monument", "", "#004040", "#424243", "color"],
    #     ["True Black / Taxi", "", "#004040", "#fb9403", "color"],
    #     ["True Black / Total Eclipse", "", "#004040", "#2c3540", "color"],
    #     ["True Navy / Bright White Mack Stripe", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_MackStripe_Navy_WHite_cf760a40-314d-4129-8c7b-4d0550cdd049_46x.jpg?v=1559765797", "#000", "", "image"],
    #     ["True Navy Crossbow", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/Crossbow_True_Navy_46x.jpeg?v=1559766104", "#000", "", "image"],
    #     ["True Navy / Dean Blue / Sunkissed", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_18hr_Weldon_United_True_Navy_Dean_Blue_Sunkissed_46x.jpg?v=1559766873", "#000", "", "image"],
    #     ["True Navy Prairie Vine", "https://cdn.shopify.com/s/files/1/0078/6825/2273/files/18hr_PrarieVine_Navy_Denim_46x.jpg?v=1559766618", "#000", "", "image"],
    #     ["Weldon Blue", "", "#7c98ab", "", "color"],
    #     ["Whisper White", "", "#f0ecec", "", "color"],
    #     ["Wild Plum", "", "#74325e", "", "color"],
    #     ["Plum Heather", "", "#412228", "", "color"],
    #     ["Spruce Heather", "", "#44503e", "", "color"],
    #     ["Gargoyle", "", "#474747", "", "color"],
    #     ["Moonstruck", "", "#e6e5e2", "", "color"],
    #     ["Laurel Canyon", "", "#77614e", "", "color"],
    #     ["Cactus", "", "#96aa9d", "", "color"],
    #     ["Sandstorm", "", "#a58e78", "", "color"],
    #     ["Covert Green", "", "#283e31", "", "color"],
    #     ["Monument Grey", "", "#c6c6c8", "", "color"],
    #     ["Indigo", "", "#203c6a", "", "color"],
    #     ["Black Sky", "", "#131313", "", "color"],
    # ]

    # colors.each do |color|
    #     SwatchColor.create(
    #         'title' => color[0],
    #         'color_image' => color[1],
    #         'color' => color[2],
    #         'color_2' => color[3],
    #         'show_type' => color[4]
    #     )
    # end
