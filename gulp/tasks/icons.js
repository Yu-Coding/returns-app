const gulp = require('gulp')
const config = require('../gulp.config')
const $ = require('gulp-load-plugins')()

gulp.task('icons', function () {
    return gulp.src(config.src.icons)
    .pipe($.changed(config.theme + '/assets', {hasChanged: $.changed.compareSha1Digest}))
    .pipe(gulp.dest(config.theme + '/assets'))
})
