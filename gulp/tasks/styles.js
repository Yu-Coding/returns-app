const gulp = require('gulp')
const config = require('../gulp.config')
const when = require('gulp-if')
const $ = require('gulp-load-plugins')()
const production = config.production
const moduleImporter = require('sass-module-importer')
const destination = config.theme + '/assets'


gulp.task('style:styles', function () {
  return gulp.src(config.src.styles + '/style.scss')
    .pipe(when(!production, $.sourcemaps.init()))
    .pipe($.sass({importer: moduleImporter()}))
    .on('error', $.sass.logError)
    .pipe($.autoprefixer({browsers: ['last 2 versions', 'iOS 8']}))
    .pipe(when(production, $.groupCssMediaQueries()))
    .pipe(when(production, $.csscomb()))
    .pipe(when(!production, $.sourcemaps.write()))
    .pipe(when(production, $.cssnano()))
    .pipe(gulp.dest(destination))
})

gulp.task('styles', gulp.series('style:styles'))
