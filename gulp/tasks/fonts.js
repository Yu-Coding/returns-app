const gulp = require('gulp')
const config = require('../gulp.config')
const $ = require('gulp-load-plugins')()

gulp.task('fonts', function () {
    return gulp.src(config.src.fonts)
    .pipe($.changed(config.theme + '/assets', {hasChanged: $.changed.compareSha1Digest}))
    .pipe(gulp.dest(config.theme + '/assets'))
})
