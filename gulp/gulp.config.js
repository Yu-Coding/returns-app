// We use this to read flags in the command line
const argv = require('yargs').argv
const production = argv.prod || argv.production

module.exports = {
  theme: './public/returns',
  src: {
    styles: './app/frontend/styles',
    scripts: './app/frontend/scripts',
    icons: [
      './app/frontend/icons/**/*'
    ],
    assets: [
      './app/frontend/assets/**/*'
    ],
    fonts: [
      './app/frontend/fonts/**/*'
    ]
  },
  scriptVendors: [
    'jquery',
    'vue',
    'vuex',
    'lodash.uniqby',
    'circletype'
  ],
  onError: function (error) {
    console.log(error.toString())
    this.emit('end')
  },
  production: !!production
}
