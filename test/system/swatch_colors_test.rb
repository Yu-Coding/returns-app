require "application_system_test_case"

class SwatchColorsTest < ApplicationSystemTestCase
  setup do
    @swatch_color = swatch_colors(:one)
  end

  test "visiting the index" do
    visit swatch_colors_url
    assert_selector "h1", text: "Swatch Colors"
  end

  test "creating a Swatch color" do
    visit swatch_colors_url
    click_on "New Swatch Color"

    fill_in "Color", with: @swatch_color.color
    fill_in "Color 2", with: @swatch_color.color_2
    fill_in "Color image", with: @swatch_color.color_image
    fill_in "Show type", with: @swatch_color.show_type
    fill_in "Title", with: @swatch_color.title
    click_on "Create Swatch color"

    assert_text "Swatch color was successfully created"
    click_on "Back"
  end

  test "updating a Swatch color" do
    visit swatch_colors_url
    click_on "Edit", match: :first

    fill_in "Color", with: @swatch_color.color
    fill_in "Color 2", with: @swatch_color.color_2
    fill_in "Color image", with: @swatch_color.color_image
    fill_in "Show type", with: @swatch_color.show_type
    fill_in "Title", with: @swatch_color.title
    click_on "Update Swatch color"

    assert_text "Swatch color was successfully updated"
    click_on "Back"
  end

  test "destroying a Swatch color" do
    visit swatch_colors_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Swatch color was successfully destroyed"
  end
end
