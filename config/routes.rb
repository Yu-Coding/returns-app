Rails.application.routes.draw do

  root :to => 'home#index'
  mount ShopifyApp::Engine, at: '/'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # FrontEnd Test
  get  '/frontend'                        => 'api/frontend#orders'

  # FlowLink integration
  post 'api/v1/flowlink/'                      => 'api/v1/flowlink#afterReceiveItems'

  # APIs for Front End
  get  'api/frontend/orders'              => 'api/frontend#getOrdersByDateRange'
  get  'api/frontend/order/:id'           => 'api/frontend#getOrder'

  post 'api/frontend/available-options'  => 'api/v1/utilities#availableOptions'
  post 'api/frontend/colors'             => 'api/v1/utilities#availableColors'
  get  'api/frontend/reasons'             => 'api/v1/utilities#returnReasons'
  get  'api/frontend/containers'          => 'api/v1/utilities#returnContainers'
  get  'api/frontend/methods'             => 'api/v1/utilities#returnMethods'

  post  'api/frontend/complete-return'    => 'api/frontend#completeReturn'
  post  'api/frontend/exchange-data'    => 'api/v1/utilities#exchangeData'
  get  'api/frontend/return-data'    => 'api/frontend#returnDetail'

  # Admin Panel Pages
  get  'order/:id/summary'       => 'home#returnSummary'
  get  'order/:id'               => 'home#order'
  get  'product/:id'             => 'home#product'

  # APIs for Admin Panel
  get  'api/v1/orders'           => 'api/v1/orders#getAllOrders'
  get  'api/v1/order/:id'        => 'api/v1/orders#getOrder'
  get  'api/v1/product/:id'      => 'api/v1/products#getProduct'
  get  '/api/v1/products/eligibles'  => 'api/v1/products#getEligibles'

  get  'api/v1/reasons'          => 'api/v1/utilities#returnReasons'
  get  'api/v1/containers'       => 'api/v1/utilities#returnContainers'
  get  'api/v1/methods'          => 'api/v1/utilities#returnMethods'

  post 'api/v1/complete-return'  => 'api/v1/utilities#completeReturn'
  post 'api/v1/confirm-return'  => 'api/v1/orders#confirm_return' # conform return at admin page.

  # 404
  get 'frontend/order/:id' => 'api/frontend#index'
  # match "*missing" => redirect("/frontend"), :via => [:get, :post]

end
