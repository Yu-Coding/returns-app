ShopifyApp.configure do |config|
  config.application_name = "Mack-Weldon-Returns"
  config.api_key = ENV["SHOPIFY_CLIENT_API_KEY"]
  config.secret = ENV["SHOPIFY_CLIENT_SECRET_KEY"]
  config.scope = "read_products,read_themes,write_themes,read_all_orders,read_orders,write_orders,read_customers,write_script_tags,read_locations,read_gift_cards, write_gift_cards"
  # config.scope = "read_products,read_themes,write_themes,read_orders,write_orders,read_customers"
  config.embedded_app = true
  config.api_version = "2019-04"
  config.session_repository = Shop
  config.scripttags = [
    {event: 'onload', src: "#{ENV['APP_HOST']}/returns/assets/script.js"}
  ]
  config.after_authenticate_job = { job: Shopify::AfterAuthenticateJob, inline: false }
end
