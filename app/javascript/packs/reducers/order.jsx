import {
  ORDERS_TOGGLE,
  ORDER_TOGGLE,
} from '../utils/types'

const initState = {
  orders: [],
  hasNext: false,
  hasPrevious: false,
  returnedVariants: [],
  order: {}
}

export default (state = initState, action) => {
  const { type, payload } = action
  if (type === ORDERS_TOGGLE) {

    return {
      ...state,
      orders: JSON.parse(JSON.stringify(payload.orders.data.edges)),
      hasNext: payload.orders.data.pageInfo.hasNextPage,
      hasPrevious: payload.orders.data.pageInfo.hasPreviousPage,
    }
  }

  if (type === ORDER_TOGGLE) {

    return {
      ...state,
      order: JSON.parse(JSON.stringify(payload.order.data)),
      returnedVariants: JSON.parse(JSON.stringify(payload.returned_variants))
    }
  }
  return state
}