import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import preloader from './preloader'
import order from './order'

export default (history) => combineReducers({
    router: connectRouter(history),
    preloader,
    order
})