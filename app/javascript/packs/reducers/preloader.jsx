import { PRELOADER, GET_ORDERS_REQUEST, GET_ORDERS_SUCCESS, GET_ORDERS_FAILURE } from '../utils/types'

const initialState = { 
  isFetching: true,
  data: []
};

export default (state = initialState, action) => {
  switch(action.type) {
    case GET_ORDERS_REQUEST: 
      return { ...state, isFetching: true, data: action.payload };
    case GET_ORDERS_SUCCESS: 
      return { ...state, isFetching: false };
    case GET_ORDERS_FAILURE: 
      return { ...state, isFetching: false };
    default: 
      return state;
  }
};