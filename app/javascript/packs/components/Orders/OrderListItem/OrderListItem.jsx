import React from 'react'
import {
  ResourceList,
  Badge,
  TextStyle,
} from '@shopify/polaris'
import * as Utils from '../../../utils/utils'

export default function OrderListItem(props) {
  const { node } = props
  const { id, name, createdAt, displayFulfillmentStatus, email, totalPrice, customer, tags, displayFinancialStatus} = node
  const order_id = id.split('/')[id.split('/').length - 1]
  const customer_exist = customer === null ? false : true
  return (
    <div className="OrderListItem">
    <ResourceList.Item id={order_id} url={`order/${order_id}`}>
        <div className="OrderListItem__Main">
          <div className="OrderListItem__Number">{ name }</div>
          <div className="OrderListItem__Date">{Utils.formatDate(createdAt, 1)}</div>
          <div className="OrderListItem__Status">
            <Badge> { Utils.capitalize(displayFinancialStatus) } </Badge>
          </div>
          <div className="OrderListItem__ShipmentStatus">
            <Badge status={ displayFulfillmentStatus.toLowerCase() === 'fulfilled' ? 'success'  : 'attention'}> { Utils.capitalize(displayFulfillmentStatus) } </Badge>
          </div>
          <div className="OrderListItem__Return"> {Utils.returnStatus(tags)} </div>
          <div className="OrderListItem__CustomerName">
            <TextStyle variation={customer_exist ? '' : 'subdued'}> {customer_exist ? customer.displayName : 'No customer' }</TextStyle>
          </div>
          <div className="OrderListItem__CustomerEmail">
            <TextStyle variation={customer_exist ? '' : 'subdued'}> {customer_exist ? customer.email : 'No customer' }</TextStyle>
          </div>
          <div className="OrderListItem__Total pr-20 text-right">${totalPrice}</div>
          <div className="OrderListItem__Quaranteee text-right"><TextStyle variation="positive">Approved</TextStyle></div>
        </div>
      </ResourceList.Item>
    </div>
  )
}