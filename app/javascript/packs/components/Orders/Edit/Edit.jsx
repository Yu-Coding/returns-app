import React, {Component} from 'react'
import { connect } from 'react-redux';
import {
  Page,
  Layout,
  Card,
  TextStyle,
  TextContainer,
  Thumbnail,
  Button,
  DisplayText,
  Checkbox,
  Modal
} from '@shopify/polaris'
import { getOrder } from '../../../actions/order'
import * as Utils from '../../../utils/utils'
import { returnMethods, returnReasons } from '../../../utils/constants'
import Returns from '../Returns'
import Loader from '../../Loader'
import CustomerInfo from '../../Customer/Info'

class Order extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      order_id: null,
      order: {},
      products: [],
      line_items: [],
      productsPrepared: [],
      selectedId: null,
      confirmTog: false,
    }

    this.selectItem = this.selectItem.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps) {
      let { returnedVariants, order } = nextProps
      let products = []
      let returnedProducts = []
      let returns = this.props.location.state && this.props.location.state.returns ? this.props.location.state.returns : []
      if (order && order.lineItems) {
        products = order.lineItems.edges.filter(edge => {
          return !returns.map(r => `gid://shopify/LineItem/${r.line_item_id}`).includes(edge.node.id)
        })

        returnedProducts = order.lineItems.edges.map(edge => {
          let return_data = returns.find(r =>  `gid://shopify/LineItem/${r.line_item_id}` === edge.node.id)
          if (return_data) {
            return {...edge, ...return_data}
          }
        }).filter(item => typeof item !== 'undefined')
        this.setState({
          line_items: order.lineItems.edges,
          products: products,
          productsPrepared: returnedProducts
        })
      }
    }
  }

  componentDidMount() {
    this.loadOrder()
  }

  loadOrder  = ()  => {
    this.props.getOrder({
      id: this.props.match.params.id,
      cb: data => {
        this.setState({loading: false})
      }
    })
  }

    // getData() {
    //     this.props.getOrder({
    //         id: this.state.order_id,
    //         cb: (data) => {
    //             let items = data.order.line_items
    //             let returns = [], products = items
    //             if (this.props.location.state) {
    //                 products = []
    //                 let info = this.props.location.state
    //                 items.forEach(item => {
    //                     if(item.product_id == info.product_id) {
    //                         // add return information to item
    //                         item.return_info = info
    //                         returns.push(item)
    //                     }
    //                     else
    //                         products.push(item)
    //                 });
    //             }
    //             this.setState({
    //                 order: data.order,
    //                 products: products,
    //                 productsPrepared: returns,
    //                 loading: false
    //             })
    //         }
    //     })
    // }

    selectItem = (checked, newValue) => {
        // let productsSelected= this.state.products.filter((item) =>
        //     (item.product_id == newValue)
        // )
        // this.setState({productsSelected: productsSelected})
        this.setState({selectedId: checked ? newValue : null})
    }

  returnProduct = (variant_id, tog, type, togAvailable) => {
    if (togAvailable == 'TOG' && type == 'refund') {
      this.setState({selectedId: variant_id, confirmTog: true})
    } else {

      let product = this.state.products.find(edge => edge.node.variant && edge.node.variant.id == variant_id)
      if (product) {
        let line_item_id = this.getIdFromGraphqlId(product.node.id)
        let variant_id = this.getIdFromGraphqlId(product.node.variant.id)
        let product_id = this.getIdFromGraphqlId(product.node.variant.product.id)
        let returns = this.props.location.state && this.props.location.state.returns ? this.props.location.state.returns : []
        this.props.history.push({
          pathname: `/product/${product_id}`,
          state: {
            order_id: this.props.match.params.id,
            line_item_id: line_item_id,
            variant_id: variant_id,
            tog: tog,
            type: type,
            returns: returns
          }
        })
      }
    }
  }
  getIdFromGraphqlId = gid => {
    return gid.split("/")[gid.split("/").length - 1]
  }

  continueReturn = () => {
    this.returnProduct(this.state.selectedId, 'STANDARD', 'refund')
  }

  cancelReturn = () => {
    this.setState({confirmTog: false})
  }

  render() {
    const {
      loading,
      products,
      productsPrepared,
      productsSelected,
      selectedId,
      confirmTog
    } = this.state

    const { order, returnedVariants } = this.props
    const { name } = order
    const returnVariantIds = returnedVariants.map(v => `gid://shopify/ProductVariant/${v.variant_id}`)

    let togAvailableForSelected = 'STANDARD'
    let togAvailableProductTitle = null

    const order_number = order.order_number ? (`#${order.order_number}`) : ''

    const productsList = ( products.length > 0 ) ? (products.map((item) => {
      const togAvailable = item.node.variant && Utils.checkTogProduct(item.node.variant.product.tags) ? 'TOG' : 'STANDARD'

      if ( item.node.variant && item.node.variant.id == selectedId) {
        togAvailableForSelected = togAvailable
        togAvailableProductTitle = item.node.name
      }
      const togStatus = ( togAvailable == 'TOG') ? (
        <Button disabled={ !item.node.variant || item.node.refundableQuantity !== 0 ? false : true } ariaExpanded={false} onClick={() => this.returnProduct(item.node.variant.id, 'TOG', 'exchange', 'STANDARD')}>TOG</Button>
      ) : null

      const returned_variant = item.node.variant && returnVariantIds.includes(item.node.variant.id) ? returnedVariants.find(v => `gid://shopify/ProductVariant/${v.variant_id}` === item.node.variant.id) : null

      const returnActions = returned_variant ? (
          <div className="return-actions">
            <p>
              <TextStyle variation="strong">Reason: {returnReasons[returned_variant.return_reason_id]}</TextStyle>
            </p>
            <p>
              <TextStyle>Method: {returnMethods[returned_variant.return_method_id]}</TextStyle>
            </p>
          </div>
        ) : (
        <div className="return-actions">
          { togStatus }
          <Button disabled={ !item.node.variant || item.node.refundableQuantity !== 0 ? false : true } ariaExpanded={false} onClick={() => this.returnProduct(item.node.variant.id, 'STANDARD', 'refund', togAvailable)}>Return</Button>
          <Button disabled={ !item.node.variant || item.node.refundableQuantity !== 0 ? false : true } ariaExpanded={false} onClick={() => this.returnProduct(item.node.variant.id, 'STANDARD', 'exchange', togAvailable)}>Exchange</Button>
        </div>
      )

      return (
        <Card key={item.node.id}>
          <div className="header-1">
            <div className="m-auto">
              <Checkbox
                checked={item.node.variant && selectedId == item.node.variant.id}
                id={item.node.variant ? item.node.variant.id : item.node.id}
                disabled={!item.node.variant}
                onChange={this.selectItem}
              />
            </div>
            <div className="d-flex" style={{ width: '80%' }}>
              <div>
                <Thumbnail
                  source={item.node.image ? item.node.image.src : 'https://cdn.shopify.com/s/images/admin/no-image-compact.gif'}
                  alt={item.node.image ? item.node.image.altText : item.node.name}
                  size="large"
                />
              </div>
              <div className="information ml-20">
                <h3>
                  <TextStyle variation="strong">{item.node.name}</TextStyle>
                </h3>
                <div>
                   <TextStyle>{item.node.sku}</TextStyle>
                </div>
                <TextStyle variation="subdued">${item.node.originalUnitPrice} X {item.node.quantity}</TextStyle>
              </div>
            </div>
            { returnActions }
          </div>
        </Card>
      )})) : null

    const returnSelectedButton = ( selectedId ) ? (
      <div className="mt-20">
        <Button primary onClick={() => this.returnProduct(selectedId, 'STANDARD', 'refund', togAvailableForSelected)}>Return Selected</Button>
      </div>
    ): null

    const confirmModal = (
      <Modal
        open={confirmTog}
        onClose={this.handleChange}
        title="Return confirmation for TOG eligible item"
        primaryAction={{
          content: 'Yes',
          onAction: this.continueReturn
        }}
        secondaryActions={[
          {
            content: 'No',
            onAction: this.cancelReturn,
          }
        ]}
      >
        <Modal.Section>
          <TextContainer>
            <p>
              <TextStyle variation="strong">{togAvailableProductTitle}</TextStyle> item is available for TOG, are you sure you want to proceed with a return?
            </p>
          </TextContainer>
        </Modal.Section>
      </Modal>
    )

    return (
      <div className="order">
        {loading && <Loader />}
        {/* {loading && <Skeleton />} */}
        {!loading &&
          <Page
            breadcrumbs={[{content: 'Back to Order Index', url: '/'}]}
            title={`Order ${name}`}
            primaryAction={{content: 'Cancel', primary: false, url: '/'}}
            fullWidth={true}
          >
            <Layout>
              <Layout.Section>
                <Card>
                  <div className="header-1">
                    <div></div>
                    <div>
                      <h3>
                        <TextStyle variation="strong">PRODUCT</TextStyle>
                      </h3>
                    </div>
                    <div>
                      <h3>
                        <TextStyle variation="strong">RETURN TYPE</TextStyle>
                      </h3>
                    </div>
                  </div>
                </Card>
                { productsList }
                {returnSelectedButton }
                {confirmModal}
                <div style={{ marginTop: '50px' }}>
                  <DisplayText size="large">Return Summary</DisplayText>
                </div>
                <Returns returns={productsPrepared} loading={loading} />
              </Layout.Section>
                <CustomerInfo order={order} />
            </Layout>
          </Page>
        }
      </div>
    )
  }
}


const mapStateToProps = state => ({
  order: state.order.order,
  returnedVariants: state.order.returnedVariants
})


const mapDispatchToProps = {
    getOrder
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Order)