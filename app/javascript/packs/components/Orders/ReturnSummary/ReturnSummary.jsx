import React, { Component } from 'react'
import { connect } from 'react-redux';
import {
  Page,
  Button,
  Layout,
  TextStyle
} from '@shopify/polaris'
import Loader from '../../Loader'
import Returns from '../Returns'
import CustomerInfo from '../../Customer/Info'
import { getOrder } from '../../../actions/order'
import * as Constants from '../../../utils/constants'

class ReturnSummary extends Component {

  constructor(props) {
    super(props)

    this.state = {
      order_id: null,
      loading: true,
      order: {},
      returns: [],
      returned_items: []
    }
  }

  componentWillMount () {
    if (this.props.location.state && this.props.location.state.products) {
      let returned_items = this.props.location.state.products.map(item => `gid://shopify/LineItem/${item.line_item_id}`)
      console.log(returned_items)
      this.setState({
        returned_items: returned_items
      })
    }
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      this.setState({
        order_id: this.props.match.params.id
      }, () => {
        this.getData()
      })
    }
  }

  getData() {
    let {returned_items} = this.state
    this.props.getOrder({
      id: this.state.order_id,
      cb: (data) => {
        let returns = data.order.data.lineItems.edges.filter(edge => returned_items.includes(edge.node.id)).map(edge => {
          let return_data = this.props.location.state.products.find(item => `gid://shopify/LineItem/${item.line_item_id}` === edge.node.id)
          return {
            ...edge,
            ...return_data
          }
        })
        this.setState({
          order: data.order.data,
          returns: returns,
          loading: false
        })
      }
    })
  }

  render () {
    const {
      loading,
      order,
      returns,
      order_id,
    } = this.state
      console.log(returns)
    return (
      <div className="ReturnSummary">
          {loading && <Loader />}
          {/* {loading && <Skeleton />} */}
          {!loading &&
            <Page
              breadcrumbs={[{content: 'Back to Order details', url: `/order/${order_id}`}]}
              title={`Return successfully processed!`}
              primaryAction={[
                  {content: 'Send Return Label', id: "send-label", url: `${Constants.SHOP_ADMIN_URL}orders/${order_id}/shipping_labels/new_return_label`, external: true},
                  {content: 'Return to Order Index', id: "return-index", url: '/'},
              ]}
              fullWidth={true}
            >
              <Layout>
                <Layout.Section>
                  <p><TextStyle variation="subdued">View the return summary below.</TextStyle></p>
                  <Returns returns={returns} loading={loading}/>
                  <div className="mt-20">
                      <Button url="/">Return to Order Index</Button>
                  </div>
                </Layout.Section>
                <CustomerInfo order={order} />
              </Layout>
            </Page>
            }
      </div>
    )
  }
}

const mapDispatchToProps = {
    getOrder
}

export default connect(
    null,
    mapDispatchToProps
)(ReturnSummary)