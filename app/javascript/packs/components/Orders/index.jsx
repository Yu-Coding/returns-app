import React, {Component} from 'react'
import { connect } from 'react-redux'
import {
    Page,
    Card,
    ResourceList,
    Pagination,
} from '@shopify/polaris';
import { getOrders } from '../../actions/order'
import * as Constants from '../../utils/constants'
import OrderListItem from './OrderListItem'
import Loader from '../Loader'

class Orders extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            orders: [],
            hasNext: false,
            hasPrevious: false,
            selectedItems: [],
            sortValue: 'ORDER_NUMBER:DESC',
            searchValue: '',
            direction: '',
            cursor: '',
            appliedFilters: [],
        }

        this.handleSearchChange = this.handleSearchChange.bind(this)
        this.handleFiltersChange = this.handleFiltersChange.bind(this)
        this.handleSortChange = this.handleSortChange.bind(this)
        this.handleSelectionChange = this.handleSelectionChange.bind(this)
    }

    componentDidMount() {
        this.getOrders()
    }

    handleSearchChange = (searchValue) => {
      this.setState({
        searchValue: searchValue
      },
        () => {
          this.getOrders()
        }
      )
    }

    handleFiltersChange = (appliedFilters) => {
        this.setState({appliedFilters}, () => {
          this.getOrders()
        })
    }

    handleSortChange = (sortValue) => {
        this.setState({
          page: 1,
          sortValue: sortValue
        }, () => {
          this.getOrders()
        })
    }

    handleSelectionChange = (selectedItems) => {
        this.setState({selectedItems})
    }

    showLoading = () => {
        this.setState({loading: true})
    }

    hideLoading = () => {
        this.setState({loading: false})
    }

    handlePage = key => {
      let { orders } = this.props
      let cursors = orders.map(order => order.cursor)
      let cursor = key === 'next' ? cursors[cursors.length - 1] : cursors[0]
      this.setState({
        direction: key,
        cursor: cursor
      }, () =>{
        this.getOrders()
      })
    }

    getOrders = () => {
      this.setState({loading: true})
      const { page, sortValue, appliedFilters, searchValue, direction, cursor } = this.state
      this.props.getOrders({
        page,
        sortValue,
        searchValue,
        appliedFilters,
        direction,
        cursor,
        cb: data => {
          this.setState({loading: false})
        }
      })
    }

    render() {
      const { loading } = this.state
      const { hasNext, hasPrevious, orders } = this.props
      const filterControl = (
        <ResourceList.FilterControl
          filters={Constants.filters}
          appliedFilters={this.state.appliedFilters}
          onFiltersChange={this.handleFiltersChange}
          searchValue={this.state.searchValue}
          onSearchChange={this.handleSearchChange}
        />
      )

      return (
        <div className="orders">
          <Page title="Index" fullWidth={true}>
            <Card>
              <ResourceList
                loading={loading}
                resourceName={Constants.resourceName}
                items={orders}
                renderItem={(order) => <OrderListItem key={order.cursor} {...order} />}
                sortValue={this.state.sortValue}
                sortOptions={Constants.sortOptions}
                onSortChange={this.handleSortChange}
                filterControl={filterControl}
                />
              <div className="pagination">
                <Pagination
                  hasPrevious={hasPrevious}
                  onPrevious={() => {this.handlePage('prev')}}
                  hasNext={hasNext}
                  onNext={() => {this.handlePage('next')}}
                />
              </div>
            </Card>
          </Page>
        </div>
      )
    }
}

const mapStateToProps = state => ({
  orders: state.order.orders,
  hasNext: state.order.hasNext,
  hasPrevious: state.order.hasPrevious
})


const mapDispatchToProps = {
  getOrders
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Orders)