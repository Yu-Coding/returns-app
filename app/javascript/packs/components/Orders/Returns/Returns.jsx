import React, {Component} from 'react'
import { withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import {
    Card,
    TextStyle,
    TextContainer,
    Heading,
    Thumbnail,
    Button,
} from '@shopify/polaris'
import * as Utils from '../../../utils/utils'
import { returnReasons }  from '../../../utils/constants'
import { returnProduct } from '../../../actions/product'

class Returns extends Component {

  constructor(props) {
    super(props);

    this.state = {
      returning: false,
      disabled: false
    }
  }

  componentDidMount() {
  }

  confirmReturn = (items) => {
    let order_id = null
    this.setState({returning: true})
    let returns = items.map(item => {
      return {
        order_id: item.order_id,
        line_item_id: item.line_item_id,
        return_type: item.type,
        normal_type: item.tog,
        product_id: item.product_id,
        reason: item.reason,
        method: item.method,
        container: item.container,
        variant_id: item.variant_id,
        exchange_product_id: item.exchange_product_id,
        exchange_variant_id: item.exchange_variant_id
      }
    })
    order_id = returns[0].order_id
    this.props.returnProduct({
      returns,
      cb: data => {
        this.setState({returning: false})
        // this.props.history.push({
        //   pathname: `/order/${order_id}/summary`,
        //   state: {
        //     products: returns
        //   }
        // })
      }
    })
    // products.forEach(item => {
    //   let info = item.return_info
    //   order_id = info.order_id
    //   let data = [{
    //     order_id: info.order_id,
    //     line_item_id: info.line_item_id,
    //     return_type: info.type,
    //     normal_type: info.tog,
    //     product_id: info.product_id,
    //     reason: info.reason,
    //     method: info.method,
    //     container: info.container,
    //     variant_id: info.variant_id
    //   }]

    //   this.props.returnProduct({
    //     data,
    //     cb: (data) => {
    //     }
    //   })
    // })
    // this.props.history.push({
    //   pathname: `/order/${order_id}/summary`,
    //   state: {
    //     products: products
    //   }
    // })
  }

    render() {
      const {
        returns,
        loading,
        details
      } = this.props
      const {returning, disabled} = this.state

      const returnsList = (returns && returns.length > 0) ? (returns.map((item) => (
        <Card.Section key={item.node.id}>
          <div className="header-2">
            <div className="d-flex">
              <div>
                <Thumbnail
                  source={item.node.image.src}
                  alt={item.node.image.altText}
                />
              </div>
                <div className="information ml-20">
                  <h3>
                    <TextStyle variation="strong">{item.node.name}</TextStyle>
                  </h3>
                  <div>
                    {/* <TextStyle variation="positive">{Utils.deliveryStatus(item.fulfillment_status).status}</TextStyle> */}
                  </div>
                  <TextStyle variation="subdued">${item.node.originalUnitPrice}</TextStyle>
                </div>
              </div>
              <div className="return-types">
                <TextContainer>
                  <Heading>RETURN TYPE</Heading>
                  <p className="mt-0">{Utils.returnType(item.tog || item.normal_type, item.type || item.return_type)}</p>
                </TextContainer>
                <TextContainer>
                  <Heading>REASON</Heading>
                  <p className="mt-0">{returnReasons[item.reason]}</p>
                </TextContainer>
              </div>
            </div>
          </Card.Section>
        ))) : (
          <Card sectioned>
            <TextStyle variation="subdued">There have been no returns or exchanges made for this order</TextStyle>
          </Card>
        )
        const returnsSummary = (returns && returns.length > 0) ? (
          <div>
            <Card>
              <Card.Section>
                <div className="header-2">
                  <div>
                    <h3>
                      <TextStyle variation="strong">PRODUCT</TextStyle>
                    </h3>
                  </div>
                  <div>
                    <h3>
                      <TextStyle variation="strong">RETURN INFORMATION</TextStyle>
                    </h3>
                  </div>
                </div>
              </Card.Section>
              { returnsList }
            </Card>
              <div className="mt-20">
                <Button
                 primary
                 disabled={!returns || returns && returns.length === 0}
                 loading={returning}
                 onClick={() => this.confirmReturn(returns)}>Confirm</Button>
              </div>
          </div>
        ) : (
            returnsList
        )

        return (
            <div className="return-summary mt-20">
                { !loading && returnsSummary }
            </div>
        )
    }
}

const mapDispatchToProps = {
  returnProduct
}

export default withRouter(connect(
    null,
    mapDispatchToProps
)(Returns))