import React from 'react'
import logo from '../../img/MW_loader.svg'

export default function Loader(props) {
    return (
        <div>
            <img src={logo} />
        </div>
    )
}
