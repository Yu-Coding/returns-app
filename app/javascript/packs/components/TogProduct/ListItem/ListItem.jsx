import React from 'react'
import {
  ResourceList,
  Badge,
  TextStyle,
  Tag
} from '@shopify/polaris'
import '@shopify/polaris/styles.css'

export default function TogProductListItem(props) {
    const {
        id,
        product_id,
        title,
        product_type,
        tags,
        color,
        size 
    } = props
    
    let tagArray = tags.split(',')
    const tagList = tagArray.map((t) => {
        return <Tag>{t.trim()}</Tag>   
    })

    return (
        <div className="TogProductListItem">
            <ResourceList.Item id={id} url={`tog-products/${id}/edit`}>
                <div className="ProductListItem__Main">
                    <div className="ProductListItem__Title">{title}</div>
                    <div className="ProductListItem__Type">{product_type}</div>
                    <div className="ProductListItem__Color">{color}</div>
                    <div className="ProductListItem__Size">{size}</div>
                </div>
            </ResourceList.Item>
        </div>
    )
}