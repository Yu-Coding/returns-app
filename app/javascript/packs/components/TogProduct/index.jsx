import React, {Component} from 'react'
import { connect } from 'react-redux';
import {
    Page,
    Card,
    ResourceList,
    Pagination
} from '@shopify/polaris'
import { getTogProducts } from '../../actions/product'
import Loader from '../Loader'
import * as Constants from '../../utils/constants'
import TogProductListItem from './ListItem'

class TogProducts extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            products: [],
            page: 1,
            has_more: false,
            selectedItems: [],
            sortValue: 'DATE_MODIFIED_DESC',
            searchValue: '',
            appliedFilters: [],
        }

        this.handleSearchChange = this.handleSearchChange.bind(this)
        this.handleFiltersChange = this.handleFiltersChange.bind(this)
        this.handleSortChange = this.handleSortChange.bind(this)
        this.handleSelectionChange = this.handleSelectionChange.bind(this)
    }

    componentDidMount() {
        this.getData(1, this.state.sortValue)
    }

    getData(page, sortValue) {
        this.showLoading()
        this.props.getTogProducts({
            page: page,
            sort: sortValue,
            cb: (data) => {
                this.setState({
                    sortValue: sortValue,
                    page: page,
                    products: data.products,
                    has_more: data.has_more,
                    loading: false
                })
            }
        })
    }

    handleSearchChange = (searchValue) => {
        this.setState({searchValue})
    }

    handleFiltersChange = (appliedFilters) => {
        this.setState({appliedFilters})
    }

    handleSortChange = (sortValue) => {
        this.getData(1, sortValue)
    }

    handleSelectionChange = (selectedItems) => {
        this.setState({selectedItems})
    }
    
    showLoading = () => {
        this.setState({loading: true})
    }

    hideLoading = () => {
        this.setState({loading: false})
    }

    previousProducts = () => {
        this.getData(this.state.page - 1, this.state.sortValue)
    }

    nextProducts = () => {
        this.getData(this.state.page + 1, this.state.sortValue)
    }

    render() {
        const {
            loading,
            products,
            page,
            has_more,
        } = this.state
        const filterControl = (
            <ResourceList.FilterControl
                filters={Constants.filters}
                appliedFilters={this.state.appliedFilters}
                onFiltersChange={this.handleFiltersChange}
                searchValue={this.state.searchValue}
                onSearchChange={this.handleSearchChange}
                additionalAction={{
                    content: 'Save',
                    onAction: () => console.log('New filter saved'),
                }}
            />
        )

        return (
            <div className="TogProducts">
                {loading && <Loader />}
                {!loading &&
                    <Page title="TOG Products" fullWidth={true}>
                        <Card>
                            <ResourceList
                                resourceName={Constants.resourceNameForTogProduct}
                                items={products}
                                renderItem={(product) => <TogProductListItem {...product} />}
                                selectedItems={this.state.selectedItems}
                                onSelectionChange={this.handleSelectionChange}
                                promotedBulkActions={Constants.promotedBulkActionsForTogProduct}
                                bulkActions={Constants.bulkActionsForTogProduct}
                                sortValue={this.state.sortValue}
                                sortOptions={Constants.sortOptions}
                                onSortChange={this.handleSortChange}
                                filterControl={filterControl}
                            />
                            <div className="Pagination">
                                <Pagination
                                    hasPrevious={page>1}
                                    onPrevious={this.previousProducts}
                                    hasNext={has_more}
                                    onNext={this.nextProducts}
                                />
                            </div>
                        </Card>
                    </Page>
                }
            </div>
        )
    }
}

const mapDispatchToProps = {
    getTogProducts
}

export default connect(
    null,
    mapDispatchToProps
)(TogProducts)