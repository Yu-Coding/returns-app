import React, {Component} from 'react'
import { connect } from 'react-redux';
import {
    Page,
} from '@shopify/polaris'
import TogProductForm from '../Form'
import Loader from '../../Loader'

class TogProductEdit extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
        }
    }

    render() {
        const {
        } = this.state

        return (
            <Page
                breadcrumbs={[{content: 'Products', url: '/tog-products'}]}
                title="TOG Product"
            >
                <TogProductForm />
            </Page>
        )
    }
}

const mapDispatchToProps = {
}

export default connect(
    null,
    mapDispatchToProps
)(TogProductEdit)