import React, {Component} from 'react'
import { connect } from 'react-redux';
import {
    Card,
    Form,
    FormLayout,
    TextField,
    TextStyle,
    Button
} from '@shopify/polaris'
import ReactTags from 'react-tag-autocomplete'
import Loader from '../../Loader'

class TogProductForm extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            type: '',
            size: '',
            color: '',
            tags: [
            ],
            suggestions: [
            ]
        }
        this.handleDelete = this.handleDelete.bind(this)
        this.handleAddition = this.handleAddition.bind(this)
    }

    handleChange = (field) => {
        return (value) => this.setState({[field]: value})
    }

    handleDelete (i) {
        const tags = this.state.tags.slice(0)
        tags.splice(i, 1)
        this.setState({ tags })
    }
    
    handleAddition (tag) {
        const tags = [].concat(this.state.tags, tag)
        this.setState({ tags })
    }

    render() {
        const {
            title,
            type,
            size,
            color
        } = this.state

        return (
            <Card sectioned>
                <Form>
                    <FormLayout>
                        <TextField
                            value={title}
                            onChange={this.handleChange('title')}
                            label="Title"
                            type="text"
                        />
                        <TextField
                            value={title}
                            onChange={this.handleChange('type')}
                            label="Type"
                            type="email"
                        />
                        <div>
                            <TextStyle>Tags</TextStyle>
                            <ReactTags
                                tags={this.state.tags}
                                suggestions={this.state.suggestions}
                                handleDelete={this.handleDelete}
                                handleAddition={this.handleAddition} 
                                allowNew={true} />
                        </div>
                        <TextField
                            value={title}
                            onChange={this.handleChange('color')}
                            label="Color"
                            type="text"
                        />
                        <TextField
                            value={title}
                            onChange={this.handleChange('size')}
                            label="Size"
                            type="text"
                        />
                        <Button submit>Submit</Button>
                    </FormLayout>
                </Form>
            </Card>
        )
    }
}

const mapDispatchToProps = {
}

export default connect(
    null,
    mapDispatchToProps
)(TogProductForm)