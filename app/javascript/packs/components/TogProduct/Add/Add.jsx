import React, {Component} from 'react'
import { connect } from 'react-redux';
import Loader from '../../Loader'

class TogProductCreate extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
        }

    }

    render() {
        <Page
            breadcrumbs={[{content: 'Products', url: '/tog-products'}]}
            title="Add TOG product"
        >
            <TogProductForm />
        </Page>
    }
}

const mapDispatchToProps = {
    
}

export default connect(
    null,
    mapDispatchToProps
)(TogProductCreate)