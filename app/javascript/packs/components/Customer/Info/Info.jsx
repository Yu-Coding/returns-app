import React, {Component, Fragment} from 'react'
import { connect } from 'react-redux';
import {
    Layout,
    Stack,
    Card,
    Avatar,
    Heading,
    Subheading,
    Link,
    TextContainer,
    TextStyle,
    Badge,
} from '@shopify/polaris'
import * as Constants from '../../../utils/constants'
import * as Utils from '../../../utils/utils'
import * as Types from '../../../utils/types'

class CustomerInfo extends Component {

    constructor(props) {
        super(props);

        this.state = {
        }

    }

    render() {
        const { order } = this.props
        const { customer } = order

        const customer_url = order.customer ? `${Constants.SHOP_ADMIN_URL}customers/${customer.id.split("/")[customer.id.split("/").length - 1]}` : null

        const delivery_status = Utils.deliveryStatus(order.displayFulfillmentStatus.toLowerCase())
        const shipmentStatus = (delivery_status.tyee == Types.SHIPPED) ? (
            <p>
                <TextStyle variation="positive">{delivery_status.status}</TextStyle>
            </p>
        ) : (
            (delivery_status.type == Types.SHIPPING_SOON) ? (
                <p className="color-orange">
                    <TextStyle>{delivery_status.status}</TextStyle>
                </p>
            ) : (
                <p>
                    <TextStyle variation="subdued">{delivery_status.status}</TextStyle>
                </p>
            )
        )

        return (
            <Layout.Section secondary>
                <Card>
                  {
                    order.customer.id
                    ? (
                      <Card.Header
                        title="Customer"
                        actions={[
                          {
                          content: 'Edit',
                          url: customer_url,
                          external: true
                        }]}
                      >
                      </Card.Header>
                    ) : (
                      <Card.Header>
                      </Card.Header>
                    )
                  }
                  <Card.Section>
                    {
                      order.customer.id
                      ? (
                        <Stack>
                          <Stack.Item>
                            <Avatar customer name="Farrah" size="medium" />
                          </Stack.Item>
                          <Stack.Item>
                            <h3>{order.customer.displayName}</h3>
                            <p>
                              {order.customer.ordersCount} orders
                            </p>
                            {(() => {
                              if (customer.tags && customer.tags.find(tag => tag.toLowerCase().includes('loyalty'))) {
                                let loyalty_tier = customer.tags.find(tag => tag.toLowerCase().includes('loyalty')).toLowerCase()
                                return (
                                  <div>
                                    <Badge status="success"> {loyalty_tier.replace('loyalty:', '')} </Badge>
                                  </div>
                                )
                              }
                            })()}
                            <div></div>
                          </Stack.Item>
                        </Stack>
                      ) : (
                        <TextStyle variation="subdued">No customer</TextStyle>
                      )
                    }
                  </Card.Section>
                  <Card.Section title="contact information">
                    {(() => {
                      if (customer) {
                        return (
                          <Fragment>
                            <Link>{customer.email}</Link>
                            <div>
                              <TextStyle variation={customer.phone ? '' : 'subdued'}>{ customer.phone ? customer.phone : 'No phone number'}</TextStyle>
                            </div>
                          </Fragment>
                        )
                      } else {
                        return (
                          <TextStyle variation="subdued">No customer</TextStyle>
                        )
                      }
                    })()}
                  </Card.Section>
                  <Card.Section title="shipping address">
                    {(() => {
                      if (order.shippingAddress) {
                        return (
                          <TextContainer spacing="tight">
                            <div> {order.shippingAddress.firstName} {order.shippingAddress.lastName}</div>
                            { order.shippingAddress.address1 &&  <div> {order.shippingAddress.address1} </div> }
                            { order.shippingAddress.address2 && <div> {order.shippingAddress.address2} </div> }
                            { order.shippingAddress.company && <div> {order.shippingAddress.company} </div> }
                            <div> { order.shippingAddress.city } {order.shippingAddress.provinceCode} {order.shippingAddress.zip} </div>
                            <div>{order.shippingAddress.country}</div>
                          </TextContainer>
                        )
                      } else {
                        return (<TextStyle variation="subdued">No shipping address</TextStyle>)
                      }
                    })()}
                  </Card.Section>
                  <Card.Section title="billing address">
                    <TextContainer>
                      {(() => {
                        if (order.billingAddress) {
                          if (order.billingAddressMatchesShippingAddress) {
                            return <TextStyle variation="subdued"> Same as shipping address </TextStyle>
                          } else {
                            return (
                              <TextContainer spacing="tight">
                                <div> {order.billingAddress.firstName} {order.billingAddress.lastName}</div>
                                { order.billingAddress.address1 &&  <div> {order.billingAddress.address1} </div> }
                                { order.billingAddress.address2 && <div> {order.billingAddress.address2} </div> }
                                { order.billingAddress.company && <div> {order.billingAddress.company} </div> }
                                <div> { order.billingAddress.city } {order.billingAddress.provinceCode} {order.billingAddress.zip} </div>
                                <div>{order.billingAddress.country}</div>
                              </TextContainer>
                            )
                          }
                        } else {
                          return (<TextStyle variation="subdued">No billing address</TextStyle>)
                        }
                      })()}
                    </TextContainer>
                  </Card.Section>
                </Card>
                <Card
                    title="Order Information"
                >
                    <Card.Section>
                        <div className="OrderInfo">
                            <TextContainer spacing="tight">
                                <Subheading>STATUS</Subheading>
                                <p>
                                  <TextStyle variation="positive">Complete</TextStyle>
                                </p>
                            </TextContainer>
                        </div>
                        <div className="OrderInfo">
                            <TextContainer spacing="tight">
                                <Subheading>SUBTOTAL</Subheading>
                                <p>
                                    <TextStyle variation="subdued">${order.subtotalPrice}</TextStyle>
                                </p>
                            </TextContainer>
                        </div>
                        <div className="OrderInfo">
                            <TextContainer spacing="tight">
                                <Subheading>TOTAL</Subheading>
                                <p>
                                    <TextStyle variation="subdued">${order.totalPrice}</TextStyle>
                                </p>
                            </TextContainer>
                        </div>
                        <div className="OrderInfo">
                          <TextContainer spacing="tight">
                            <Subheading>CHECKOUT</Subheading>
                            <p>
                              {
                                order.checkoutId
                                ? (
                                  <TextStyle></TextStyle>
                                ) : (
                                  <TextStyle variation="subdued">-</TextStyle>
                                )
                              }
                            </p>
                          </TextContainer>
                        </div>
                        <div className="OrderInfo">
                          <TextContainer spacing="tight">
                            <Subheading>IP ADDRESS</Subheading>
                            <p className="color-orange">
                              {
                                order.clientIp
                                ? (
                                  <TextStyle> {order.clientIp} </TextStyle>
                                ) : (
                                  <TextStyle>Pending</TextStyle>
                                )
                              }
                            </p>
                          </TextContainer>
                        </div>
                        <div className="OrderInfo">
                            <TextContainer spacing="tight">
                                <Subheading>SHIPMENT</Subheading>
                                { shipmentStatus }
                            </TextContainer>
                        </div>
                        <div className="OrderInfo">
                            <TextContainer spacing="tight">
                                <Subheading>PAYMENT</Subheading>
                                <p>
                                    <TextStyle variation="positive">{Utils.capitalize(order.displayFinancialStatus)}</TextStyle>
                                </p>
                            </TextContainer>
                        </div>
                        <div className="OrderInfo">
                          <TextContainer spacing="tight">
                            <Subheading>DATE COMPLETED</Subheading>
                            <p>
                              <TextStyle variation="subdued"></TextStyle>
                            </p>
                          </TextContainer>
                        </div>
                    </Card.Section>
                    <Card.Section>
                        <TextStyle variation="positive">Guarantee approved</TextStyle>
                    </Card.Section>
                </Card>
            </Layout.Section>
        )
    }
}

const mapDispatchToProps = {
}

export default connect(
    null,
    mapDispatchToProps
)(CustomerInfo)