import React, {Component} from 'react'
import { connect } from 'react-redux';
import {
    Page,
    Card,
    Checkbox,
    Layout,
    Form,
    FormLayout,
    TextField,
    Select,
    Stack,
    RadioButton,
    Loading
} from '@shopify/polaris'
import { getProduct, getEligibles } from '../../../actions/product'
import { getReasons, getMethods, getContainers } from '../../../actions/utils'
import * as Utils from '../../../utils/utils'
import Loader from '../../Loader'

class Product extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      order_id: null,
      line_item_id: null,
      variant_id: null,
      id: 0,
      product: {},
      tog: 'STANDARD',
      type: 'exchange',
      fabric: '',
      style: '',
      size: '',
      color: '',
      reason: 1,
      method: 1,
      container: 1,
      locations: [],
      location: '',
      styles: [],
      sizes: [],
      colors: [],
      reasons: [],
      methods: [],
      containers: [],
      fabrics: [],
      returns: [],
      locations: [],
      location: '',
      memo: '',
      third_party_email: '',
      hide_from_customer: true
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleChangeReimbursementType = this.handleChangeReimbursementType.bind(this)
    this.handleChangeType = this.handleChangeType.bind(this)
    this.handleChangeFabric = this.handleChangeFabric.bind(this)
    this.handleChangeStyle = this.handleChangeStyle.bind(this)
    this.handleChangeSize = this.handleChangeSize.bind(this)
    this.handleChangeColor = this.handleChangeColor.bind(this)
    this.handleChangeReason = this.handleChangeReason.bind(this)
    this.handleChangeMethod = this.handleChangeMethod.bind(this)
    this.handleChangeContainer = this.handleChangeContainer.bind(this)
  }

  componentDidMount() {
    if (this.props.location.state) {
      let {
        order_id,
        line_item_id,
        variant_id,
        tog,
        type,
        returns
      } = this.props.location.state
      this.setState({
        id: this.props.match.params.id,
        order_id: order_id,
        line_item_id: line_item_id,
        variant_id: variant_id,
        tog: tog,
        type: type,
        returns: returns
      }, () => {
        this.getProductData()
      })
    }
  }

  getProductData() {
  let {id, tog, type} = this.state
    this.props.getProduct({
      id,
      tog,
      type,
      cb: (data) => {
        let locations = data.locations.map(location => {
          return {
            label: location.name,
            value: location.id.toString()
          }
        })
        console.log(data.methods)
        this.setState({
          product: data.product,
          products: data.products,
          fabrics: data.fabric_options,
          fabric: data.fabric_options[0] ? data.fabric_options[0].value : '',
          styles: data.style_options,
          style: data.style_options[0],
          containers: data.containers,
          reasons: data.reasons,
          methods: data.methods,
          locations: locations,
          location: locations[0] ? locations[0].value : '',
          loading: false
        }, () => {
          this.setElgibleOptions(data.products)
          // this.setVariants(this.state.tog, this.state.type, data.product)
        })
      }
    })
  }

  getEligibles = () => {
    const { style, fabric } = this.state
    this.props.getEligibles({
      style,
      fabric,
      cb: data => {
        this.setElgibleOptions(data.products)
      }
    })
  }

  setElgibleOptions = products => {
    let sizes =[]
    let colors = []
    const { tog, product } = this.state
    if (products.length === 0) return false
    if (tog === 'TOG') {
      products.data.edges.map(edge => {
        let size_option = edge.node.options.find(option => option.name.toLowerCase() === 'size')
        let color_option = edge.node.options.find(option => option.name.toLowerCase() === 'color')
        if (size_option) sizes = [...sizes, ...size_option.values]
        if (color_option) colors = [...colors, ...color_option.values]
      })
    } else {
      let size_option = product.options.find(option => option.name.toLowerCase() === 'size')
      let color_option = product.options.find(option => option.name.toLowerCase() === 'color')
      if (size_option) sizes = size_option.values
      if (color_option) colors = color_option.values
    }
    sizes = [...new Set(sizes)]
    colors = [...new Set(colors)]
    this.setState({
      sizes: sizes,
      size: sizes[0],
      colors: colors,
      color: colors[0]
    })
  }

    // Change Reimbursement Type

  handleChangeReimbursementType = (selected) => {
    this.setState({
      type: selected
    })
  }

    // Change Type
  handleChangeType = ( checked, newValue ) => {
    this.setState({
      type: newValue
    })
  }

    // Change Fabric
  handleChangeFabric = (selected) => {
    this.setState({
      fabric: selected
    }, () => {
      this.getEligibles()
    })
  }

    // Change Style
  handleChangeStyle = (selected) => {
    this.setState({
      style: selected
    }, () => {
      this.getEligibles()
    })
  }

    // Change Size
  handleChangeSize = (selected) => {
    this.setState({
      size: selected
    })
  }

    // Change Color
  handleChangeColor = (selected) => {
    this.setState({
      color: selected
    })
  }

    // Change exchange/refund reason
  handleChangeReason = ( selected ) => {
    this.setState({
      reason: selected
    })
  }

    // Change refund method
  handleChangeMethod = ( selected ) => {
    this.setState({
      method: selected
    })
  }

    // Change container
  handleChangeContainer = ( checked, newValue ) => {
    this.setState({
      container: newValue
    })
  }

  handleChange = (property) => value => {
    this.setState({[property]: value});
  }

  saveToList = () => {
    let { color, size, products, order_id, line_item_id, product, type, tog, reason, method, container, variant_id, returns, location, memo, third_party_email, hide_from_customer } = this.state
    let variants = []
    let exchange_variant_id, exchange_product_id
    if (type == 'exchange') {
      if (tog === 'TOG') {
        products.data.edges.map(edge => {
          variants = [...variants, ...edge.node.variants.edges]
        })

        let variant = variants.find(v => v.node.selectedOptions[0].value === color && v.node.selectedOptions[1].value === size )
        exchange_variant_id = variant.node.id.split('/')[variant.node.id.split('/').length - 1]
        exchange_product_id = variant.node.product.id.split('/')[variant.node.product.id.split('/').length - 1]
      } else {
        let variant = product.variants.find(v => v.option1 === color && v.option2 === size)
        exchange_product_id = product.id
        exchange_variant_id = variant.id
      }
    }

    let return_data = {
      order_id: order_id,
      line_item_id: line_item_id,
      variant_id: variant_id,
      product_id: product.id,
      type: type,
      tog: tog,
      reason: Number(reason),
      method: Number(method),
      container: container,
      exchange_product_id: exchange_product_id,
      exchange_variant_id: exchange_variant_id
    }

    if (tog !== 'TOG' && type == 'exchange') {
      return_data['stock_info'] = {
        location: Number(location),
        memo: memo,
        third_party_email: third_party_email,
        hide_from_customer: hide_from_customer
      }
    }

    this.props.history.push({
      pathname: `/order/${order_id}`,
      state: {
        returns: [
          ...returns,
          return_data
        ]
      }
    })
  }

  render() {
    const {
      loading,
      order_id,
      product,
      tog,
      type,
      fabric,
      style,
      size,
      color,
      reason,
      method,
      container,
      reasons,
      styles,
      sizes,
      colors,
      methods,
      containers,
      fabrics,
      locations,
      location,
      memo,
      third_party_email,
      hide_from_customer
    } = this.state

    const reimbursement_type_options = ( tog == 'TOG' ) ? [
      {label: 'TOG', value: 'TOG'},
    ]: [
      {label: 'Exchange', value: 'exchange'},
      {label: 'Return', value: 'refund'},
    ]
    const reimbursementTypesSelect = (
      <Layout>
        <Layout.Section oneThird>
          <div style={{ width: '30%' }}>
            <Select
              label="Reimbursement Type"
              disabled={tog=='TOG'}
              options={reimbursement_type_options}
              onChange={this.handleChangeReimbursementType}
              value={(tog=='TOG') ? this.state.tog : this.state.type}
            />
          </div>
        </Layout.Section>
      </Layout>
    )
    const reimbursementTypesRadio = (tog == 'TOG') ? (
      <FormLayout>
        <Stack horizontal>
          <RadioButton
            label="Exchange"
            checked={type=='exchange'}
            id="exchange"
            name="type"
            onChange={this.handleChangeType}
          />
            <RadioButton
              label="Refund"
              checked={type=='refund'}
              id="refund"
              name="type"
              onChange={this.handleChangeType}
            />
        </Stack>
      </FormLayout>
    ) : null
    const fabricList = ( tog == 'TOG' && type == 'exchange' ) ? (
      <Select
        label="Fabric"
        options={fabrics}
        onChange={this.handleChangeFabric}
        value={fabric}
      />
    ) : null
    const styleList = ( tog == 'TOG' && type == 'exchange' ) ? (
      <Select
        label="Style"
        options={styles}
        onChange={this.handleChangeStyle}
        value={style}
      />
    ) : null
    const sizeList = ( type == 'exchange' ) ? (
      <Select
        label="Size"
        options={sizes}
        onChange={this.handleChangeSize}
        value={size}
      />
    ) : null
    const colorList = ( type == 'exchange' ) ? (
      <Select
        label="Color"
        options={colors}
        onChange={this.handleChangeColor}
        value={color}
      />
    ) : null
    const exchangeOptionsList = ( type == 'exchange' ) ? (
      <div style={{ marginBottom: 30 }}>
        <FormLayout>
          <FormLayout.Group condensed>
            { fabricList }
            { styleList }
            { sizeList }
            { colorList }
          </FormLayout.Group>
        </FormLayout>
      </div>
    ) : null

    const return_reasons = reasons.map(reason => {
      return {
        label: reason.label,
        value: reason.value.toString()
      }
    })
    const reasonsList = ( reasons.length > 0 ) ? (
      <FormLayout>
        <div style={{ width: '30%' }}>
          <Select
            label={ ( type == 'exchange' ) ? "Exchange reason" : "Refund reason" }
            options={return_reasons}
            onChange={this.handleChangeReason}
            value={this.state.reason}
          />
        </div>
      </FormLayout>
    ) : null

    const return_methods = methods.map(method => {
      return {
        label: method.label,
        value: method.value.toString()
      }
    })

    const methodsList = ( type == 'refund' && methods.length > 0 ) ? (
      <FormLayout>
        <div style={{ width: '30%' }}>
          <Select
            label="Refund method"
            options={return_methods}
            onChange={this.handleChangeMethod}
            value={this.state.method}
          />
        </div>
      </FormLayout>
    ) : null
    const containersList = ( tog == 'STANDARD' ) ? (
      <div className="ContainerList">
        {containers.map((ct) => (
          <div key={ct.value} className="mt-0">
            <RadioButton
              label={Utils.capitalize(ct.label)}
              checked={container === ct.value}
              id={ct.value}
              name="container"
              onChange={this.handleChangeContainer}
            />
          </div>
        ))}
      </div>
    ) : null
    const stockInformation = ( tog == 'STANDARD' ) ? (
      <Card sectioned>
        <FormLayout>
          <Stack>
            <Select
              label="Stock location"
              options={locations}
              onChange={this.handleChange('location')}
              value={location}
            />
          </Stack>
          <Select
            label="Reason"
            options={return_reasons}
            onChange={this.handleChangeReason}
            value={this.state.reason}
          />
          <TextField
            label="Memo"
            value={memo}
            onChange={this.handleChange('memo')}
            multiline={5}
            type="text"
            style={{ width: '50%' }}
          />
          <TextField
            label="Third Party Email"
            value={third_party_email}
            type="email"
            onChange={this.handleChange('third_party_email')}
          />
          <Checkbox
            checked={hide_from_customer}
            label="Hide from customer"
            onChange={this.handleChange('hide_from_customer')}
          />
        </FormLayout>
      </Card>
    ) : null
    return (
      <div className="ProductReturn">
        {loading && <Loader />}
        {/* {loading && <Skeleton />} */}
        {!loading &&
          <Page
            breadcrumbs={[{content: 'Back to Order Details', url: `/order/${order_id}`}]}
            title={`${product.title ? product.title : ''}`}
            primaryAction={[
                {content: 'Cancel', id: "cancel", url: `/order/${order_id}`},
                {content: 'Save', id: "save", onAction: this.saveToList, disabled: (type == 'exchange' && !size && !color)},
            ]}
            fullWidth={true}
          >
            <Card sectioned>
              <Form>
                { reimbursementTypesSelect }
                { reimbursementTypesRadio }
                { exchangeOptionsList }
                { reasonsList }
                { methodsList }
                { containersList }
              </Form>
            </Card>
            {stockInformation}
          </Page>
        }
      </div>
    )
  }
}

const mapDispatchToProps = {
  getProduct,
  getEligibles
}

export default connect(
  null,
  mapDispatchToProps
)(Product)