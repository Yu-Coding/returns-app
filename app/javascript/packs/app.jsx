import React, {Component} from 'react'
import { withRouter } from 'react-router'
import { connect } from 'react-redux'
import { Switch, Route } from 'react-router-dom'
import { Frame } from '@shopify/polaris'
import routes from './routes'

// wrapping/composing
const FadingRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
      <Component {...props}/>
  )}/>
)

class App extends Component {
  render() {
    return (
      <Frame>
        <Switch>
          {routes.map((route) => (
            <FadingRoute
              key={route.path} 
              path={route.path} 
              component={route.component} 
              exact={route.exact} 
            />
          ))}
        </Switch>
      </Frame>
    )
  }
}

const mapStateToProps = ({ preloader }) => ({
  preloader: preloader.isFetching
})

export default withRouter(connect(mapStateToProps)(App));