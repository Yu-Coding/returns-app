import React from 'react'
import ReactDOM from 'react-dom'
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'
import { AppProvider } from '@shopify/polaris'
import { store, history } from './store'
import App from './app'

if( document.readyState === 'complete' ) {
    render()
} else {
    document.addEventListener('DOMContentLoaded', () => {
        render()
    })
}

function render() {
    ReactDOM.render(
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <AppProvider>
                    <App />
                </AppProvider>
            </ConnectedRouter>
        </Provider>,
        document.body.appendChild(document.createElement('div'))
    )
}