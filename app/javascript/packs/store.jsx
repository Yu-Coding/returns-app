import { createBrowserHistory } from 'history'
import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import thunkMiddleware from 'redux-thunk'
import createSagaMiddleware from 'redux-saga'
import createRootReducer from './reducers'
import rootSaga from './saga'

const initalState = {}
const sagaMiddleware = createSagaMiddleware()

export const history = createBrowserHistory()

export const store = createStore(
    createRootReducer(history), 
    initalState, 
    compose(
        applyMiddleware(
            routerMiddleware(history),
            thunkMiddleware,
            sagaMiddleware
        )
    )
)

sagaMiddleware.run(rootSaga)

