import regeneratorRuntime from 'regenerator-runtime'
import { call, takeLatest } from 'redux-saga/effects'
import request from '../utils/request'
import { 
    GET_REASONS,
    GET_METHODS,
    GET_CONTAINERS
} from '../utils/types'

function* getReasons({ data }) {
    let res
    try {
        res = yield call(
            request.get,
            'reasons'
        )
    } catch(error) {
    } finally {
        if (res) {
            data.cb && data.cb(res.data)
        }
    }
}

function* getMethods({ data }) {
    let res
    try {
        res = yield call(
            request.get,
            `methods?tog=${data.tog}&type=${data.type}`
        )
    } catch(error) {
    } finally {
        if (res) {
            data.cb && data.cb(res.data)
        }
    }
}

function* getContainers({ data }) {
    let res
    try {
        res = yield call(
            request.get,
            'containers'
        )
    } catch(error) {
    } finally {
        if (res) {
            data.cb && data.cb(res.data)
        }
    }
}

export function* utils() {
    yield takeLatest(GET_REASONS, getReasons)
    yield takeLatest(GET_METHODS, getMethods)
    yield takeLatest(GET_CONTAINERS, getContainers)
}