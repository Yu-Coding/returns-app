import regeneratorRuntime from 'regenerator-runtime'
import { call, takeLatest, put } from 'redux-saga/effects'
import request from '../utils/request'
import {
  GET_ORDERS,
  GET_ORDER,
  ORDERS_TOGGLE,
  ORDER_TOGGLE
} from '../utils/types'

function* getOrders({ data }) {
  let res
  try {
    res = yield call(
      request.get,
      `orders`,
      {params: data}
    )
  } catch(error) {
    console.log(error)
  } finally {
    if (res) {
      yield put({
        type: ORDERS_TOGGLE,
        payload: {
          ...res.data
        }
      })
      data.cb && data.cb(res.data)
    }
  }
}

function* getOrder({ data }) {
    let res
    try {
        res = yield call(
            request.get,
            `order/${data.id}`
        )
    } catch(error) {
    } finally {
        if (res) {
          yield put({
            type: ORDER_TOGGLE,
            payload: {
              ...res.data
            }
          })
          data.cb && data.cb(res.data)
        }
    }
}

export function* order() {
    yield takeLatest(GET_ORDERS, getOrders)
    yield takeLatest(GET_ORDER, getOrder)
}