import regeneratorRuntime from 'regenerator-runtime'
import { all } from 'redux-saga/effects'
import { order } from './order'
import { product } from './product'
import { utils } from './utils'

export default function* rootSaga() {
    yield all([
        order(),
        product(),
        utils()
    ])
}