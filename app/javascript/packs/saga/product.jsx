import regeneratorRuntime from 'regenerator-runtime'
import { call, takeLatest } from 'redux-saga/effects'
import request from '../utils/request'
import {
    GET_PRODUCT,
    RETURN_PRODUCT,
    GET_TOG_PRODUCTS,
    CREATE_TOG_PRODUCT,
    GET_TOG_PRODUCT,
    UPDATE_TOG_PRODUCT,
    DELETE_TOG_PRODUCT,
    LOAD_ELIGIBLE_PRODUCTS
} from '../utils/types'

function* getProduct({ data }) {
    let res
    try {
        res = yield call(
            request.get,
            `product/${data.id}`,
            {params: data}
        )
    } catch(error) {
    } finally {
        if (res) {
            data.cb && data.cb(res.data)
        }
    }
}

function* returnProduct({ data }) {
    let res
    try {
        res = yield call(
            request.post,
            'confirm-return',
            data
        )
    } catch(error) {
    } finally {
        if (res) {
            data.cb && data.cb(res.data)
        }
    }
}

function* getTogProducts({ data }) {
    let res
    try {
        res = yield call(
            request.get,
            'tog-products',
            data
        )
    } catch(error) {
    } finally {
        if (res) {
            data.cb && data.cb(res.data)
        }
    }
}

function* createTogProduct({ data }) {
    let res
    try {
        res = yield call(
            request.post,
            'tog-products',
            data
        )
    } catch(error) {
    } finally {
        if (res) {
            data.cb && data.cb(res.data)
        }
    }
}

function* getTogProduct({ data }) {
    let res
    try {
        res = yield call(
            request.get,
            `tog-products/${data.id}`,
        )
    } catch(error) {
    } finally {
        if (res) {
            data.cb && data.cb(res.data)
        }
    }
}

function* updateTogProduct({ data }) {
    let res
    try {
        res = yield call(
            request.put,
            `tog-products/${data.id}`,
        )
    } catch(error) {
    } finally {
        if (res) {
            data.cb && data.cb(res.data)
        }
    }
}

function* deleteTogProduct({ data }) {
    let res
    try {
        res = yield call(
            request.delete,
            `tog-products/${data.id}`,
        )
    } catch(error) {
    } finally {
        if (res) {
            data.cb && data.cb(res.data)
        }
    }
}

function* loadEligibles({ data }) {
    let res
    try {
        res = yield call(
            request.get,
            `/products/eligibles`,
            {params: data}
        )
    } catch(error) {
    } finally {
        if (res) {
            data.cb && data.cb(res.data)
        }
    }
}

export function* product() {
    yield takeLatest(GET_PRODUCT, getProduct)
    yield takeLatest(RETURN_PRODUCT, returnProduct)
    yield takeLatest(GET_TOG_PRODUCTS, getTogProducts)
    yield takeLatest(CREATE_TOG_PRODUCT, createTogProduct)
    yield takeLatest(GET_TOG_PRODUCT, getTogProduct)
    yield takeLatest(UPDATE_TOG_PRODUCT, updateTogProduct)
    yield takeLatest(DELETE_TOG_PRODUCT, deleteTogProduct )
    yield takeLatest(LOAD_ELIGIBLE_PRODUCTS, loadEligibles )
}