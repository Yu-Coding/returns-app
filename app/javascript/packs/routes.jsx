import Orders from './components/Orders'
import Order from './components/Orders/Edit'
import Product from './components/Products/Edit'
import ReturnSummary from './components/Orders/ReturnSummary'

// Tog products management
import TogProductAdd from './components/TogProduct/Add'
import TogProductEdit from './components/TogProduct/Edit'
import TogProducts from './components/TogProduct'

const routes = [
    {
        path: '/',
        exact: true,
        component: Orders
    },
    {
        path: '/order/:id/summary',
        exact: false,
        component: ReturnSummary
    },
    {
        path: '/order/:id',
        exact: false,
        component: Order
    },
    {
        path: '/product/:id',
        exact: false,
        component: Product
    },
    {
        path: '/tog-products',
        exact: true,
        component: TogProducts
    },
    {
        path: '/tog-products/new',
        exact: true,
        component: TogProductAdd
    },
    {
        path: '/tog-products/:id/edit',
        exact: false,
        component: TogProductEdit
    },
]

export default routes