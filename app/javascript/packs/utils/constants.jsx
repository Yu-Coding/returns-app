import {
    FilterType
} from '@shopify/polaris'

export const SHOP_ADMIN_URL = 'https://mack-weldon.myshopify.com/admin/'

export const sortOptions = [
    {label: 'Order number (ascending)', value: 'ORDER_NUMBER:ASC'},
    {label: 'Order number (descending)', value: 'ORDER_NUMBER:DESC'},
    {label: 'Date (oldest first)', value: 'CREATED_AT:ASC'},
    {label: 'Date (newest first)', value: 'CREATED_AT:DESC'},
    {label: 'Customer name (A-Z)', value: 'CUSTOMER_NAME:ASC'},
    {label: 'Customer name (Z-A)', value: 'CUSTOMER_NAME:DESC'},
    {label: 'Payment status (A-Z)', value: 'FINANCIAL_STATUS:ASC'},
    {label: 'Payment status (Z-A)', value: 'FINANCIAL_STATUS:DESC'},
    {label: 'Fulfillment status (A-Z)', value: 'FULFILLMENT_STATUS:ASC'},
    {label: 'Fulfillment status (Z-A)', value: 'FULFILLMENT_STATUS:DESC'},
    {label: 'Total price (low to high)', value: 'TOTAL_PRICE:ASC'},
    {label: 'Total price (high to low)', value: 'TOTAL_PRICE:DESC'},
]

export const resourceName = {
    singular: 'order',
    plural: 'orders',
}

export const resourceNameForTogProduct = {
    singular: 'TOG product',
    plural: 'TOG products',
}

export const promotedBulkActions = [
    {
        content: 'Edit orders',
        onAction: () => console.log('Todo: implement bulk edit'),
    },
]

export const promotedBulkActionsForTogProduct = [
    {
        content: 'Edit products',
        onAction: () => console.log('Todo: implement bulk edit'),
    },
]

export const bulkActions = [
    {
        content: 'Add tags',
        onAction: () => console.log('Todo: implement bulk add tags'),
    },
    {
        content: 'Remove tags',
        onAction: () => console.log('Todo: implement bulk remove tags'),
    },
    {
        content: 'Delete orders',
        onAction: () => console.log('Todo: implement bulk delete'),
    },
]

export const bulkActionsForTogProduct = [
    {
        content: 'Add tags',
        onAction: () => console.log('Todo: implement bulk add tags'),
    },
    {
        content: 'Remove tags',
        onAction: () => console.log('Todo: implement bulk remove tags'),
    },
    {
        content: 'Delete products',
        onAction: () => console.log('Todo: implement bulk delete'),
    },
]

export const filters = [
    {
        key: 'status',
        label: 'Status',
        type: FilterType.Select,
        options: ['open', 'archived', 'canceled'],
    },

    {
        key: 'payment_status',
        label: 'Payment status',
        type: FilterType.Select,
        options: ['authorized', 'paid', 'partially refunded', 'partially paid', 'pending', 'refunded', 'unpaid', 'voided', 'any'],
    },
    {
        key: 'fulfillment_status',
        label: 'Fulfillment status',
        type: FilterType.Select,
        options: ['fulfilled', 'unfulfilled', 'partially fulfilled', 'unfulfilled and partially fulfilled'],
    },
    {
        key: 'tag',
        label: 'Tagged with',
        type: FilterType.TextField,
    }
]

export const returnReasons = [
    'Item too large',
    'Item too small',
    'Quality not as expected',
    'Not the right style',
    'No longer wanted or needed',
    'Ordered by mistake',
    'Incorrect item received'
]

export const returnMethods = [
    'Refund as store credit',
    'Refund my original payment',
    'Return & refund as store credit',
    'Return & refund my original payment '
]