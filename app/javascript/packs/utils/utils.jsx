import moment from 'moment'
import * as Types from './types'

export const formatDate = (date, type) => {
    let dt = date;
    switch(type) {
        case 1:
            dt = moment(String(date)).format('YYYY-MM-DD')
            break;
        case 2:
            dt = moment(String(date)).format('MMMM DD, YYYY hh:mm A')
            break;
        default:
            break;
    }
    return dt;
}

export const orderDate = (value) => {
    if(value) {
        // check if date is today
        if( moment().diff(value, 'days') == 0 ) {

        }
        // check if date is yesterday
        else if( moment().diff(value, 'days') == 1) {
            return "Yesterday at " + moment(String(value)).format('h:mm a')
        }
        // check if date is this year
        // else
        else {
            return moment(String(value)).format('MMM D') + ' at ' + moment(String(value)).format('h:mm a')
        }

    }
}

export const customerName = (customer) => {
    if(customer) {
        let first_name = customer.first_name ? customer.first_name : ''
        let last_name = customer.last_name ? customer.last_name : ''
        return first_name + " " + last_name
    } else {
        return "No customer"
    }
}

export const deliveryStatus = (status) => {
    let delivery_status = {}
    switch (status) {
        case 'fulfulled':
            delivery_status = {
                type: Types.DELIVERED,
                status: 'Delivered'
            }
            break;
        case 'partial':
            delivery_status = {
                type: Types.SHIPPED,
                status: 'Shipped'
            }
            break;
        case 'restocked':
            delivery_status = {
                type: Types.RESTOCKED,
                status: 'Restocked'
            }
            break;
        default:
            delivery_status = {
                type: Types.SHIPPING_SOON,
                status: 'Shipping Soon'
            }
            break;
    }
    return delivery_status
}

export const fulfillmentStatus = (status) => {
    if(!status)
        return 'attention'
}

export const returnStatus = (tags) => {
  return tags.filter(tag => tag.includes('R-')).join('')
}

export const completedDate = (fulfillments) => {
    if (! fulfillments.length) return '-'
    else {
      return formatDate(fulfillments[0].created_at, 2)
    }
}

export const customerEmail = (email) => {
    if(email) {
        return (email.length <= 30) ? email : (email.substring(0,30) + '...')
    } else {
        return 'No customer'
    }
}

export const capitalize = (value) => {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase()
}

export const capitalizeAll = (value) => {
    if (!value) return ''
    value = value.toString()
    return value.toUpperCase()
}

export const checkTogProduct = (tags) => {
  if (tags.length === 0)
    return false
  return tags.some((tag) => (
    tag.includes('TOG')
  ))
}

export const returnType = (tog, type) => {
  return (tog == 'TOG') ? 'TOG' : capitalizeAll(type)
}