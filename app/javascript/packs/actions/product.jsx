import {
    GET_PRODUCT,
    RETURN_PRODUCT,
    GET_TOG_PRODUCTS,
    CREATE_TOG_PRODUCT,
    UPDATE_TOG_PRODUCT,
    DELETE_TOG_PRODUCT,
    LOAD_ELIGIBLE_PRODUCTS
} from '../utils/types'

export const getProduct = ( data ) => ({
    type: GET_PRODUCT,
    data: data
})

export const returnProduct = ( data ) => ({
    type: RETURN_PRODUCT,
    data: data
})

export const getTogProducts = ( data ) => ({
    type: GET_TOG_PRODUCTS,
    data: data
})

export const createTogProduct = ( data ) => ({
    type: CREATE_TOG_PRODUCT,
    data: data
})

export const updateTogProduct = ( data ) => ({
    type: UPDATE_TOG_PRODUCT,
    data: data
})

export const deleteTogProduct = ( data ) => ({
    type: DELETE_TOG_PRODUCT,
    data: data
})

export const getEligibles = ( data ) => ({
    type: LOAD_ELIGIBLE_PRODUCTS,
    data: data
})
