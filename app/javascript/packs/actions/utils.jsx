import { 
    GET_REASONS,
    GET_METHODS,
    GET_CONTAINERS
} from '../utils/types'

export const getReasons = ( data ) => ({
    type: GET_REASONS,
    data: data
})

export const getMethods = ( data ) => ({
    type: GET_METHODS,
    data: data
})

export const getContainers = ( data ) => ({
    type: GET_CONTAINERS,
    data: data
})

