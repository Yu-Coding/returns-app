import { 
    GET_ORDERS,
    GET_ORDER,
} from '../utils/types'

export const getOrders = ( data ) => ({
    type: GET_ORDERS,
    data: data
})

export const getOrder = ( data ) => ({
    type: GET_ORDER,
    data: data
})