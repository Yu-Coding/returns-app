class Order < ApplicationRecord
    validates_uniqueness_of :order_id

    def self.createNew(id)
        return create(order_id: id, status: 'exchanged')
    end
end
