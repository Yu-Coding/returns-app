class ReimbursementType < ApplicationRecord
  def self.type_id(type)
    ['tog-refund', 'tog-exchange', 'standard-refund', 'standard-exchange'].find_index(type) + 1
  end
end
