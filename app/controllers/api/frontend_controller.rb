require 'date'

class Api::FrontendController < ApplicationController
  include ApplicationHelper

  skip_before_action :verify_authenticity_token

  def index
  end

  # get orders by date range
  def getOrdersByDateRange

      if params[:shop].present?
          set_shop(params[:shop])
          if @shop.nil?
              orders = []
              page = 1
              count = 1
              max_page = 1
          else
              activate_shopify_session

              # get params value
              page = params[:page].to_i
              range = params[:range]
              customer_id = params[:customer_id]

              limit = 3
              options = {
                  status: 'any',
                  limit: limit,
                  page: page,
                  # order: "processed_at DESC",
                  customer_id: customer_id,
                  # ids: '1219828809800', # QA
                  # ids: '1260102549617' # Live
              }
              count_options = {
                  status: 'any',
                  customer_id: customer_id,
                  # ids: '1219828809800', # QA
                  # ids: '1260102549617' # Live
              }
              options = getOrdersOptionsByRange(range, options)
              orders = getDetailedOrders(options)

              # get max page
              count = ShopifyAPI::Order.count(count_options)
              count > page * limit ? has_more = true : has_more = false
              max_page = (count.to_f / 3).ceil

              clear_shopify_session
          end
      else
          orders = []
          page = 1
          count = 1
          max_page = 1
      end

      render json: {orders: orders, page: page, count: count, max_page: max_page}
  end

  # get order by id: Front End
  def getOrder
      id = params[:id]
      set_shop(params[:shop])
      activate_shopify_session

      order = ShopifyAPI::Order.find(:first, params: {
          ids: id,
          status: 'any'
      })

      order.line_items.each do |item|
          item.extra = {}
          begin
              product = ShopifyAPI::Product.find(item.product_id, params: {
                  fields: 'image,images,variants,options,tags,product_type'
              })

              item.extra['images'] = product.images
              image = product.images.find{|image| image.variant_ids.include?(item.variant_id)}
              item.extra['image'] = image.present? ? image.src : product.image.present? ? product.image.src : 'https://cdn.shopify.com/s/images/admin/no-image-compact.gif'
              item.extra['variants'] = product.variants { |variant| variant.price == item.price }
              item.extra['options'] = product.options
              item.extra['tags'] = product.tags

              # get product type
              item.extra['tog_info'] = getTogInfo(product, item.price)
              item.extra['product_type'] = product.product_type
              item.extra['return_info'] = getReturnInfo(order.id, item.variant_id)
          rescue ActiveResource::ResourceNotFound => e
              item.extra['tog_info'] = getTogInfo(nil, item.price)
              item.extra['product_type'] = nil
              item.extra['return_info'] = getReturnInfo(nil, nil)
          end

          begin
              variant = ShopifyAPI::Variant.find(item.variant_id, params: {
                  fields: 'option1,option2'
              })
              item.extra['color'] = variant.option1
              item.extra['size'] = variant.option2
          rescue ActiveResource::ResourceNotFound => e
          end
      end

      render json: {order: order}
  end

  def exchangeData
    render json: {order: {}, product: {}}
  end

  def set_shop(domain)
      @shop = Shop.where(shopify_domain: domain).take
  end

  def activate_shopify_session
      session = ShopifyAPI::Session.new(domain: @shop.shopify_domain, token: @shop.shopify_token, api_version: '2019-04')
      ShopifyAPI::Base.activate_session(session)
  end

  def clear_shopify_session
      ShopifyAPI::Base.clear_session
  end
end
