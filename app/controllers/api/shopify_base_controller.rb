# class Api::ShopifyBaseController < ShopifyApp::AuthenticatedController
class Api::ShopifyBaseController < ApplicationController
  before_action :set_shop

  def set_shop
    # @shop = Shop.where(shopify_domain: session[:shopify_domain]).take
    @shop = Shop.where(shopify_domain: 'mack-weldon-qa.myshopify.com').take

    # puts '=============== checking start ==============='
    # puts @shop.to_json
    # puts '=============== checking end ==============='

  end

  def activate_shopify_session
    session = ShopifyAPI::Session.new(domain: @shop.shopify_domain, token: @shop.shopify_token, api_version: :unstable)
    ShopifyAPI::Base.activate_session(session)
  end

  def clear_shopify_session
    ShopifyAPI::Base.clear_session
  end

end
