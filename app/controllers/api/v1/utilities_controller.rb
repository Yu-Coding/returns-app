class Api::V1::UtilitiesController < ApplicationController
    include ApplicationHelper

    skip_before_action :verify_authenticity_token

    def returnReasons
        @reasons = ReturnReason.select('id as value, reason as label')
        render json: {reasons: @reasons}
    end

    def returnMethods
        normal_type = params[:tog]
        return_type = params[:type]
        reimbursement_type_id = ReimbursementType.where({normal_type: normal_type, return_type: return_type}).first.id
        @methods = ReturnMethod.order('id ASC').select('id as value, description as label').where({reimbursement_type_id: reimbursement_type_id})
        render json: {methods: @methods}
    end

    def returnContainers
        @containers = ReturnContainer.select('id as value, container as label')
        render json: {containers: @containers}
    end

    def availableColors
        shop = params[:shop]
        colors = params[:colors]
        handle = params[:handle]

        url = "https://#{shop}/products/#{handle}"
        render json: {colors: curl(url, colors)}
    end

    def completeReturn
        if params[:shop].present?
            set_shop_frontend(params[:shop])
            if @shop.nil?
            else
                activate_shopify_session
            end
        else
            set_shop
            # activate_shopify_session
        end

        if @shop.present?
            data = params[:data]
            # DB ids
            @return_info_ids = Array.new

            types = Array.new

            @tr_original_variant_ids, @tr_exchange_variant_ids = Array.new(2) { [] }
            @te_original_variant_ids, @te_exchange_variant_ids = Array.new(2) { [] }
            @se_original_variant_ids, @se_exchange_variant_ids = Array.new(2) { [] }

            @tr_reasons = String.new

            @order_id = data[0][:order_id]
            @original_order = get_order(@order_id)

            data.each do |value|
                @param = value
                get_return_data
                get_reimbursement_data

                save_return_data

                @type = get_reimbursement_type(@reimbursement_data[:normal_type], @reimbursement_data[:return_type])
                types << @type

                if @type == Constants::TOG_REFUND
                    reason = ReturnReason.find(@reimbursement_data[:reason]).reason
                    tr_reason = "#{get_product_title(@return_data[:product_id])}(#{reason})"

                    if @tr_original_variant_ids.present?
                        @tr_reasons << ", "
                    end
                    @tr_reasons << tr_reason

                    @tr_original_variant_ids << @return_data[:variant_id]
                    @tr_exchange_variant_ids << @return_data[:exchange_variant_id]

                elsif @type == Constants::TOG_EXCHANGE
                    @te_original_variant_ids << @return_data[:variant_id]
                    @te_exchange_variant_ids << @return_data[:exchange_variant_id]

                # elsif @type == Constants::STANDARD_REFUND

                elsif @type == Constants::STANDARD_EXCHANGE
                    @se_original_variant_ids << @return_data[:variant_id]
                    @se_exchange_variant_ids << @return_data[:exchange_variant_id]

                    get_customer_data
                    get_customer_token
                end
            end

            if types.include? Constants::TOG_REFUND
                items_text = items_text(@tr_original_variant_ids)
                reason = "TOG-Refund: #{items_text(@tr_original_variant_ids)} - #{@tr_reasons}"
                puts reason

                # refund
                create_refund(@original_order, @tr_original_variant_ids, reason)

                type_id = get_reimbursement_type_id Constants::TOG_REFUND
                save_order_info(@order_id, nil, type_id, 'TOG-Refund')
            elsif types.include? Constants::TOG_EXCHANGE
                # create an order with exchange items
                new_native_order = create_shopify_order(@original_order, @te_original_variant_ids, @te_exchange_variant_ids, 'paid', 'TOG-Exchange')

                # mark orignal order as refunded/partially refunded
                mark_order_as_refunded(@original_order, @te_original_variant_ids)

                type_id = get_reimbursement_type_id Constants::TOG_EXCHANGE
                save_order_info(@order_id, new_native_order.id, type_id, 'TOG-Exchange')
            elsif types.include? Constants::STANDARD_REFUND
                type_id = get_reimbursement_type_id Constants::STANDARD_REFUND
                save_order_info(@order_id, nil, type_id, 'STANDARD-Refund')
            elsif types.include? Constants::STANDARD_EXCHANGE
                # create an order with exchange items
                new_native_order = create_shopify_order(@original_order, @se_original_variant_ids, @se_exchange_variant_ids, 'pending', 'STANDARD-Exchange')

                type_id = get_reimbursement_type_id Constants::STANDARD_EXCHANGE
                save_order_info(@order_id, new_native_order.id, type_id, 'STANDARD-Exchange')

                customer = get_customer(@original_order)
                if customer.present?
                    stripe_customer_id = create_stripe_customer(@token, customer.email).id
                    save_customer_info(customer.id, stripe_customer_id, new_native_order.id, @customer_data)
                else
                    create_stripe_customer(@token, nil)
                    save_customer_info(nil, stripe_customer_id, new_native_order.id, @customer_data)
                end
            end
        end

        if params[:shop].present?
            if @shop.present?
                clear_shopify_session
            end
        # else
        #     if @shop.present?
        #         clear_shopify_session
        #     end
        end

        return_infos = Product.where(id: @return_info_ids)

        render json: { return_infos: return_infos }
    end

    def get_return_data
        @return_data = @param.permit(
            :order_id,
            :order_number,
            :line_item_id,
            :product_id,
            :variant_id,
            :sku,
            :quantity,
            :exchange_variant_id,
        )
    end

    def get_reimbursement_data
        @reimbursement_data = @param.permit(
            :normal_type,
            :return_type,
            :reason,
            :method,
            :container,
            :line_item_id
        )
        @reimbursement_type = ReimbursementType.where({
            return_type: @reimbursement_data[:return_type] ,
            normal_type: @reimbursement_data[:normal_type]
        }).first
    end

    def get_customer_data
        @customer_data = @param.require(:customer).permit(:name, :address1, :address2, :city, :state, :zip_code, :country)
    end

    def get_customer_token
        @token = @param[:customer][:token]
    end

    def save_return_data
        @product = Product.create @return_data
        if @product.present?
            info = {
                :exchange_product_id => get_product_id_from_variant_id(@return_data[:exchange_variant_id]),
                :return_id => "RA#{formatted_number(@product.id)}",
                :reimbursement_type_id => @reimbursement_type.id,
                :return_reason_id => @reimbursement_data[:reason],
                :return_method_id => @reimbursement_data[:method],
                :return_container_id => @reimbursement_data[:container]
            }
            @product.update info

            @return_info_ids << @product.id
        end
    end

    ##### Shopiy API #####
    def set_shop_frontend(domain)
        @shop = Shop.where(shopify_domain: domain).take
    end

    def set_shop
        @shop = Shop.where(shopify_domain: session[:shopify_domain]).take
    end

    def activate_shopify_session
        session = ShopifyAPI::Session.new(domain: @shop.shopify_domain, token: @shop.shopify_token, api_version: '2019-04')
        ShopifyAPI::Base.activate_session(session)
    end

    def clear_shopify_session
        ShopifyAPI::Base.clear_session
    end

end
