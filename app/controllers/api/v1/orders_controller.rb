require 'date'

class Api::V1::OrdersController < Api::ShopifyBaseController
    include ApplicationHelper

    # skip_before_action :verify_authenticity_token
    before_action :activate_shopify_session
    after_action :clear_shopify_session

end
