class Api::V1::FlowLinkController < ApplicationController
    include ApplicationHelper

    skip_before_action :verify_authenticity_token
    before_action :activate_shopify_session
    after_action :clear_shopify_session

    # FlowLink integration - after receiving item
    def after_receive_items
        get_params

        products_info = Product.where(order_number: @order_number)
        if products_info.count > 0
            order_id = products_info.first.order_id
            # mark items as received
            @skus = Array.new
            @line_items.each do |item|
                mark_product_as_received(item.sku, item.quantity)
                @skus << item.sku
            end

            product_ids, variant_ids = Array.new(2) { [] }

            orders = Order.where({order_id: order_id})

            orders.each do |order|
                reimbursement_type = ReimbursementType.find(order.reimbursement_type_id)
                type = get_reimbursement_type(reimbursement_type.normal_type, reimbursement_type.return_type)

                if type == Constants::STANDARD_REFUND
                    # get all product_ids which require original refund
                    available_methods = ReturnMethod.where(reimbursement_id: order_status)
                    available_methods.each do |av_method|
                        method = get_payment_method(av_method)
                        if method == Constants::STANDARD_STORE_REFUND
                            store_p_ids = Product.where(order_id: order_id).where(return_method_id: av_method.id)
                            line_items.each do |item|
                                if store_p_ids.include? item.product_id
                                    product_ids.push item.product_id
                                    variant_ids.push item.variant_id
                                end
                            end
                            create_refund_old(type, Constants::STORE_METHOD, order_id, get_line_items(order_id, variant_ids))
                        elsif method == Constants::STANDARD_ORIGINAL_REFUND
                            original_p_ids = Product.where(order_id: order_id).where(return_method_id: av_method.id)
                            line_items.each do |item|
                                if original_p_ids.include? item.product_id
                                    product_ids.push item.product_id
                                    variant_ids.push item.variant_id
                                end
                            end
                            create_refund_old(type, Constants::ORIGINAL_METHOD, order_id, get_line_items(order_id, variant_ids))
                        end
                    end
                elsif type == Constants::STANDARD_EXCHANGE
                    products = Product.where(sku: @skus).where(order.reimbursement_type_id)
                    mark_order_as_paid(new_order_id, amount)
                    # mark_order_as_refunded_old(Constants::STANDARD_EXCHANGE, order_id)
                    # new_order_id = order.new_order_id
                end
            end
        end
    end

    def get_params
        @order_number = params[:order_number]
        @line_items = params[:line_items]
        @returned_at = params[:returned_at]
    end

    # mark product as received
    def mark_product_as_received(sku, quantity)
        Product.where(sku: sku).first.update(received_quantity: quantity)
    end
end
