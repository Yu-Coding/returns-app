class Api::V1::ProductsController < Api::ShopifyBaseController
  include ApplicationHelper

  skip_before_action :verify_authenticity_token
  before_action :activate_shopify_session
  after_action :clear_shopify_session

  def getProduct
    id = params[:id]
    puts params.to_json
    product = ShopifyAPI::Product.find(:first, params:{ids: id, fields: 'id,product_type,options,title,variants,tags'})
    fabric_options = []
    style_options = []
    eligible_products = []
    locations = []
    if ['underwear', 'boxer briefs'].include?(product.product_type.downcase) || product.title.downcase.include?('under')
      fabric_options = FABRIC_UNDERWEAR_OPTIONS
      style_options = UNDERWEAR_STYLES
    elsif product.product_type.downcase == 'socks'
      fabric_options = FABRIC_SOCK_OPTIONS
      style_options = SOCKS_STYLES
    end
    if ['underwear', 'socks'].include?(product.product_type.downcase)
      eligible_products = get_graphql_product(product_graphql_query_by_tags([fabric_options[0][:value], style_options[0]]))
    end

    if params[:tog].downcase == 'standard'
      locations = ShopifyAPI::Location.find(:all)
    end
    reimbursement_type_ids = ReimbursementType.where({normal_type: params[:tog].to_s}).map { |e|  e.id}
    @methods = ReturnMethod.where({reimbursement_type_id: reimbursement_type_ids}).select('id as value, description as label')
    puts @methods.to_json

    render json: {
      product: product,
      fabric_options: fabric_options,
      style_options: style_options,
      products: eligible_products,
      containers: ReturnContainer.select('id as value, container as label'),
      reasons: ReturnReason.select('id as value, reason as label'),
      methods: @methods,
      locations: locations
    }
  end

  def getEligibles
    products = get_graphql_product(product_graphql_query_by_tags([params[:fabric], params[:style]]))
    render json: {
      products: products
    }
  end

  private
    def get_graphql_product product_graphql_query
      ShopifyGraphQLClient.client.allow_dynamic_queries = true
      result = ShopifyGraphQLClient.query(ShopifyGraphQLClient.parse(product_graphql_query))
      result.data.products
    end

    def product_graphql_query_by_tags tags
      query = 'first:2, query:"' + tags.map{|e| 'tag:' + e }.join(' AND ') + '"'
      "{
        products(#{query}) {
          edges {
            node {
              id
              handle
              options(first: 50) {
                name
                position
                values
              }
              tags
              variants(first: 100) {
                edges {
                  node {
                    selectedOptions {
                      name
                      value
                    }
                    product {
                      id
                    }
                    id
                  }
                }
              }
            }
          }
        }
      }"

    end

end
