# frozen_string_literal: true

class HomeController < Api::ShopifyBaseController
# class HomeController < ApplicationController

  def index

  end

  def order
  end

  def returnSummary
  end

  def togProducts
  end

  def createTogProduct
  end

  def editTogProduct
  end
end
