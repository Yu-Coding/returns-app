module Api
  module V1
    module Orders
      module Return
        extend ActiveSupport::Concern

        included do

          def confirm_return
            puts return_request_params.to_json
            @order = get_order(return_request_params.first[:order_id])
            render json: {}, status: 404 and return false if @order.nil?

            @tog_returns = return_request_params.select { |e| e[:return_type].downcase == 'refund'  && e[:normal_type].downcase == 'tog'}
            @tog_exchanges = return_request_params.select { |e| e[:return_type].downcase == 'exchange'  && e[:normal_type].downcase == 'tog'}
            @standard_returns = return_request_params.select { |e| e[:return_type].downcase == 'refund'  && e[:normal_type].downcase == 'standard'}
            @standard_exchange = return_request_params.select { |e| e[:return_type].downcase == 'exchange'  && e[:normal_type].downcase == 'standard'}

            process_tog_returns
            process_tog_exchanges
            process_standard_returns
            process_standard_exchanges

            create_db_logs

            render json: {}
          end

          private
            def return_request_params
              params.require(:returns)
            end

            def process_tog_returns
              return false if @tog_returns.length == 0
              process_order_refund
            end

            def process_tog_exchanges
              return false if @tog_exchanges.length == 0
              mark_order_refund
              create_new_order('paid', @tog_exchanges, 'tog-exchange')
            end

            def process_standard_returns
              return false if @standard_returns.length == 0
            end

            def process_standard_exchanges
              return false if @standard_exchange.length == 0
              create_new_order('pending', @standard_exchange, 'standard-exchange')
            end

            def create_db_logs
              data = return_request_params.map { |e| {
                order_id: @order.id,
                order_number: @order.number,
                line_item_id: e[:line_item_id],
                product_id: e[:product_id],
                exchange_product_id: e[:exchange_product_id],
                variant_id: e[:variant_id],
                exchange_variant_id: e[:exchange_variant_id],
                sku: line_item(e[:line_item_id]).sku,
                reimbursement_type_id: ReimbursementType.type_id("#{e[:normal_type].downcase}-#{e[:return_type].downcase}"),
                return_reason_id: e[:reason],
                return_method_id: e[:method],
                return_container_id: e[:container],
                quantity: line_item(e[:line_item_id]).quantity,
                processed: e[:normal_type].downcase == 'tog' ? true : false
              }}
              data.each do |d|
                record = Product.new(d)
                if record.save
                  record.update_attributes(return_id: "RA#{formatted_number(record.id)}")
                end
              end
            end

            def process_order_refund

              original_refund_line_items = @tog_returns.select{|e| e[:method].to_i == 2}
              store_credit_refund_line_items = @tog_returns.select{|e| e[:method].to_i == 1}

              original_refund_ids = original_refund_line_items.map { |e| e[:line_item_id].to_i }
              original_refund_ids = @tog_returns.map { |e| e[:line_item_id].to_i }

              if original_refund_ids.length > 0
                line_items = @order.line_items.select{|e| original_refund_ids.include?(e.id.to_i)}
                amount = line_items.sum { |e| e.price.to_f - e.total_discount.to_f }

                refund = ShopifyAPI::Refund.new({
                  :order_id => @order.id,
                  :notify => true,
                  :note => 'Tog-Refund',
                  :shipping => { :full_refund => true },
                  :refund_line_items => refund_calculation(line_items).refund_line_items,
                  :transactions => refund_calculation(line_items).transactions.map { |e| {
                    :parent_id => e.parent_id,
                    :amount => e.amount,
                    :kind => 'refund',
                    :gateway => e.gateway
                  }}
                })

                unless refund.save
                  puts refund.errors.full_messages
                end
              end

              if store_credit_refund_line_items.length > 0
                  ids = store_credit_refund_line_items.map { |e| e[:line_item_id].to_i }
                  amount = @order.line_items.select{|e| ids.include?(e.id.to_i)}.sum { |e| e.price.to_f - e.total_discount.to_f }
                  refund = ShopifyAPI::Transaction.new ({
                    amount: amount,
                    kind: 'refund',
                    order_id: @order.id
                  })
                  unless refund.save
                    puts refund.errors.full_messages
                  end
                  create_gift_card_to_customer(amount)
              end

            end

            def mark_order_refund
              ids = @tog_exchanges.map { |e| e[:line_item_id].to_i }
              amount = @order.line_items.select{|e| ids.include?(e.id.to_i)}.sum { |e| e.price.to_f - e.total_discount.to_f }
              refund = ShopifyAPI::Transaction.new ({
                amount: amount,
                kind: 'refund',
                order_id: @order.id
              })
              unless refund.save
                puts refund.errors.full_messages
              end
            end

            def line_item line_item_id
              line_item = @order.line_items.find{|e| e.id.to_i == line_item_id.to_i }
              line_item
            end

            def create_new_order financial_status, old_order_lines, type
              ids = old_order_lines.map { |e| e[:line_item_id].to_i }
              line_items = @order.line_items.select{|e| ids.include?(e.id.to_i)}
              amount = line_items.sum { |e| e.price.to_f - e.total_discount.to_f }
              order = ShopifyAPI::Order.new(
                :financial_status => financial_status,
                :transactions => [
                    {
                        :amount => amount,
                        :kind => "authorization",
                        :status => "success"
                    }
                ],
                :line_items => line_items.map{|e| {quantity: e.quantity, variant_id: e.variant_id}},
                :tags => "R-#{@order.id}, #{type}"
              )
              if @order.customer
                order.customer = {
                  id: @order.customer.id
                }
              end
              if order.save
                create_db_order_record(@order.id, order.id, type)
              end
            end
        end
      end
    end
  end
end
