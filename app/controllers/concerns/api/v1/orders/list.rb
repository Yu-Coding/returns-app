module Api
  module V1
    module Orders
      module List
        extend ActiveSupport::Concern

        included do

          # get orders list
          def getAllOrders
            orders = []
            orders = get_graphql_orders
            render json: {orders: orders, hasNext: false, hasPrevious: false}
          end

          def get_graphql_orders
            ShopifyGraphQLClient.client.allow_dynamic_queries = true
            result = ShopifyGraphQLClient.query(ShopifyGraphQLClient.parse(orders_graphql_query))
            result.data.orders
          end

          private
            def orders_get_params
              params.except(:cb, :controller, :action)
            end

            def orders_query
              if orders_get_params[:direction] == 'prev'
                query = 'last: 40, before:' '"' + "#{orders_get_params[:cursor]}" + '"'
              elsif orders_get_params[:direction]  == 'next'
                query = 'first: 40, after:' '"' + "#{orders_get_params[:cursor]}" + '"'
              else
                query = 'first: 40'
              end

              sortKey = orders_get_params[:sortValue].split(":").first
              reverse = orders_get_params[:sortValue].split(":").last.downcase == 'desc' ? 'true' : 'false'
              filter_string = ""
              if orders_get_params[:appliedFilters].present?
                orders_get_params[:appliedFilters].each do |filter|
                  filter = JSON.parse(filter)
                  filter_string = "AND #{filter['key']}:#{filter['value'].downcase} #{filter_string}"
                end
              end
              query = query + ', sortKey: ' + sortKey + ', reverse: ' + reverse + ', query: "' + orders_get_params[:searchValue] + '* ' + filter_string +'"'
              query
            end

            def orders_graphql_query
              "{
                orders(#{orders_query}) {
                  pageInfo {
                    hasNextPage
                    hasPreviousPage
                  }
                  edges {
                    node {
                      id
                      customer {
                        email
                        displayName
                      }
                      name
                      tags
                      totalPrice
                      createdAt
                      displayFulfillmentStatus
                      displayFinancialStatus
                    }
                    cursor
                  }
                }
              }
            "
          end
        end
      end
    end
  end
end
