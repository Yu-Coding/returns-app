module Api
  module V1
    module Orders
      module Show
        extend ActiveSupport::Concern

        included do

          # get order by id(Admin Panel)
          def getOrder

            # products = []
            order = get_graphql_order
            id = params[:id]
            returned_variants = Product.where(:order_id => id)

            # order = ShopifyAPI::Order.find(:all, params: {
            #   ids: id,
            #   status: 'any'
            # }).first

            # gorder.line_items.edges.each do |node|
            #   puts node.to_json
            # end

            # order.line_items.each do |item|
            #   products.push item
            #   begin
            #       product = ShopifyAPI::Product.find(item.product_id, params: {
            #           fields: 'image,images,variants,options,tags,handle'
            #       })
            #       item.variants = product.variants
            #       item.options = product.options
            #       item.image = product.image ? product.image.src : ''
            #       item.images = product.images
            #       item.tags = product.tags
            #       item.handle = product.handle
            #   rescue ActiveResource::ResourceNotFound => e

            #   end
            #   begin
            #       variant = ShopifyAPI::Variant.find(item.variant_id, params: {
            #           fields: 'option1,option2'
            #       })
            #       item.color = variant.option1
            #       item.size = variant.option2
            #   rescue ActiveResource::ResourceNotFound => e
            #   end
            # end

            render json: {order: order, returned_variants: returned_variants}
          end

          def get_graphql_order
            ShopifyGraphQLClient.client.allow_dynamic_queries = true
            result = ShopifyGraphQLClient.query(ShopifyGraphQLClient.parse(order_graphql_query))
            result.data.order
          end

          private
            def order_get_params
              params.except(:cb, :controller, :action)
            end

            def order_graphql_query
              order_query = '"gid://shopify/Order/' + order_get_params[:id].to_s + '"'
              "
              {
                order(id: #{order_query}) {
                  note
                  billingAddress {
                    zip
                    provinceCode
                    province
                    phone
                    name
                    longitude
                    latitude
                    lastName
                    id
                    formattedArea
                    company
                    city
                    country
                    countryCode
                    firstName
                    address2
                    address1
                  }
                  shippingAddress {
                    zip
                    provinceCode
                    province
                    phone
                    name
                    longitude
                    latitude
                    lastName
                    id
                    formattedArea
                    company
                    city
                    country
                    countryCode
                    firstName
                    address2
                    address1
                  }
                  billingAddressMatchesShippingAddress
                  canMarkAsPaid
                  cancelReason
                  cancelledAt
                  capturable
                  cartDiscountAmount
                  clientIp
                  closedAt
                  closed
                  confirmed
                  createdAt
                  name
                  customer {
                    displayName
                    email
                    id
                    ordersCount
                    tags
                    phone
                    defaultAddress {
                      address1
                      address2
                      city
                      company
                      country
                      countryCode
                      firstName
                      formattedArea
                      lastName
                      name
                      phone
                      province
                      provinceCode
                      zip
                    }
                  }
                  email
                  displayFinancialStatus
                  displayFulfillmentStatus
                  fulfillable
                  id
                  fullyPaid
                  netPayment
                  phone
                  riskLevel
                  totalPrice
                  totalReceived
                  totalRefunded
                  totalTax
                  unpaid
                  subtotalPrice
                  totalShippingPrice
                  lineItems(first: 10) {
                    edges {
                      node {
                        canRestock
                        fulfillmentStatus
                        image(maxWidth: 80, maxHeight: 80) {
                          altText
                          src
                        }
                        id
                        name
                        originalTotal
                        originalUnitPrice
                        quantity
                        refundableQuantity
                        sku
                        title
                        variant {
                          id
                          product {
                            tags
                            id
                          }
                        }
                        variantTitle
                      }
                    }
                  }
                }
              }
              "
            end
        end
      end
    end
  end
end
