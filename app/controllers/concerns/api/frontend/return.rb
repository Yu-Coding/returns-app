module Api
  module Frontend
    module Return
      extend ActiveSupport::Concern

      included do

        def completeReturn
          set_shop(params[:shop])
          activate_shopify_session
          retrive_order

          set_reimbursement_type

          map_return_data

          process_tog_return          if return_params[:normal_type].downcase == 'tog'      && return_params[:return_type] == 'refund'
          process_tog_exchange        if return_params[:normal_type].downcase == 'tog'      && return_params[:return_type] == 'exchange'
          process_standard_return     if return_params[:normal_type].downcase == 'standard' && return_params[:return_type] == 'refund'
          process_standard_exchange   if return_params[:normal_type].downcase == 'standard' && return_params[:return_type] == 'exchange'

          create_db_log

          render json: {}
        end

        private
          def return_params
            params.require(:data).permit!
          end

          def process_tog_return
            puts '----tog --- return ----'
            line_item = @order.line_items.find{|item| item.id === return_params[:line_item_id]}
            amount = line_item.price.to_f - line_item.total_discount.to_f
            if return_params[:method] == 1
              refund = ShopifyAPI::Transaction.new ({
                amount: amount,
                kind: 'refund',
                order_id: @order.id
              })
              unless refund.save
                puts refund.errors.full_messages
              end
              create_gift_card_to_customer(amount)
            else
              line_items = @order.line_items.select{|item| item.id === return_params[:line_item_id]}
              refund = ShopifyAPI::Refund.new({
                :order_id => @order.id,
                :notify => true,
                :note => 'Tog-Refund',
                :shipping => { :full_refund => true },
                :refund_line_items => refund_calculation(line_items).refund_line_items,
                :transactions => refund_calculation(line_items).transactions.map { |e| {
                  :parent_id => e.parent_id,
                  :amount => e.amount,
                  :kind => 'refund',
                  :gateway => e.gateway
                }}
              })

              unless refund.save
                puts refund.errors.full_messages
              end
            end
          end

          def process_tog_exchange
            puts '----tog--exchange---'
            mark_order_refund
            create_new_order('paid', 'tog-exchange')
          end

          def process_standard_return
            puts @return_data.to_json
          end

          def process_standard_exchange
            puts '----standard--exchange---'
            create_customer_record

            create_new_order('pending', 'standard-exchange')
          end

          def set_reimbursement_type
            @reimbursement_type_id = ReimbursementType.type_id("#{return_params[:normal_type].downcase}-#{return_params[:return_type].downcase}")
          end

          def map_return_data
            @return_data = {
              order_id:       return_params[:order_id],
              order_number:   return_params[:order_number],
              line_item_id:   return_params[:line_item_id],
              product_id:     return_params[:product_id],
              variant_id:     return_params[:variant_id],
              sku:            return_params[:sku],
              quantity:       return_params[:quantity],
              exchange_product_id:    return_params[:exchange_product_id],
              exchange_variant_id:    return_params[:exchange_variant_id],
              reimbursement_type_id:  @reimbursement_type_id,
              return_reason_id:       return_params[:reason],
              return_method_id:       return_params[:method],
              return_container_id:    return_params[:container],
              processed: return_params[:normal_type].downcase == 'tog' ? true : false
            }.compact
          end

          def create_db_log
            record = Product.new(@return_data)
            if record.save
              record.update_attributes(return_id: "RA#{formatted_number(record.id)}")
            end
          end

          def create_customer_record
            set_customer_data
            stripe_customer = create_stripe_customer(return_params[:customer][:token], @customer_data)
            store_customer_data_to_db(stripe_customer.id)
          end

          def retrive_order
            @order = ShopifyAPI::Order.find(return_params[:order_id])
          end

          def set_customer_data
            @customer_data = {
              email: (@order.email if @order.email.present?),
              description: "exchange guarantee",
              address: {
                city: return_params[:customer][:city],
                line1: return_params[:customer][:address1],
                line2: (return_params[:customer][:address2] if return_params[:customer][:address2].present?),
                state: return_params[:customer][:state],
                country: return_params[:customer][:country],
                postal_code: return_params[:customer][:zip_code],
              }.compact
            }.compact
          end

          def store_customer_data_to_db stripe_customer_id
            customer_data = {
              customer_id: (@order.customer.id if @order.customer.present?),
              stripe_id: stripe_customer_id,
              order_id: return_params[:order_id],
              name: return_params[:customer][:name],
              country: return_params[:customer][:country],
              state: return_params[:customer][:state],
              city: return_params[:customer][:city],
              zip_code: return_params[:customer][:zip_code],
              address1: return_params[:customer][:address1],
              address2: return_params[:customer][:address2],
              line_item_id: return_params[:customer][:line_item_id]
            }.compact

            customer = Customer.new(customer_data)
            unless customer.save
              puts customer.errors.full_messages
            end
          end

          def create_new_order financial_status, type
            line_item = @order.line_items.find{|item| item.id === return_params[:line_item_id]}
            amount = line_item.price.to_f - line_item.total_discount.to_f
            order = ShopifyAPI::Order.new(
              :financial_status => financial_status,
              :transactions => [
                {
                  :amount => amount,
                  :kind => "authorization",
                  :status => "success"
                }
              ],
              :line_items => [{quantity: line_item.quantity, variant_id: return_params[:exchange_variant_id]}],
              :tags => "R-#{@order.id}, #{type}"
            )
            if @order.customer
              order.customer = {
                id: @order.customer.id
              }
            end
            if order.save
              create_db_order_record(@order.id, order.id, type)
            else
              puts order.errors.full_messages
            end
          end

          def mark_order_refund
            line_item = @order.line_items.find{|item| item.id === return_params[:line_item_id]}
            amount = line_item.price.to_f - line_item.total_discount.to_f
            refund = ShopifyAPI::Transaction.new ({
              amount: amount,
              kind: 'refund',
              order_id: @order.id
            })
            unless refund.save
              puts refund.errors.full_messages
            end
          end
      end
    end
  end
end
