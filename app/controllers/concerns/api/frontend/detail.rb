module Api
  module Frontend
    module Detail
      extend ActiveSupport::Concern

      included do

        def returnDetail
          set_shop(params[:shop])
          activate_shopify_session

          return_data = JSON.parse(Product.find_by_return_id(params[:return_id]).to_json)
          return_data['reason'] = ReturnReason.find(return_data['return_reason_id']).reason
          return_data['method'] = ReturnMethod.find(return_data['return_method_id']).description if return_data['return_method_id'].present?
          return_data['container'] = ReturnContainer.find(return_data['return_container_id']).container if return_data['return_container_id'].present?

          return_variant = get_graphql_variant(return_data['variant_id'])
          exchange_variant = get_graphql_variant(return_data['exchange_variant_id']) if return_data['exchange_variant_id'].present?
          return_data['variant'] = return_variant
          return_data['exchange_variant'] = exchange_variant if exchange_variant.present?

          puts return_data.to_json

          render json: {return_data: return_data}
        end

        private
          def get_graphql_variant variant_id
            ShopifyGraphQLClient.client.allow_dynamic_queries = true
            result = ShopifyGraphQLClient.query(ShopifyGraphQLClient.parse(graphql_variant_query(variant_id)))
            result.data.product_variant
          end

          private
            def graphql_variant_query variant_id
              query = '"gid://shopify/ProductVariant/' + variant_id.to_s + '"'
              "
              {
                productVariant(id: #{query}) {
                  image(maxWidth: 150, maxHeight: 150) {
                    src
                  }
                  title
                  selectedOptions {
                    name
                    value
                  }
                  displayName
                  id
                  price
                  product {
                    title
                  }
                }
              }
              "
            end
      end
    end
  end
end
