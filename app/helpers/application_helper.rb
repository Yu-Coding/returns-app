module ApplicationHelper
    include Constants
    include V1::VariantsHelper
    include V1::ProductsHelper
    include V1::OrdersHelper
    include V1::RefundsHelper
    include V1::StripeHelper
end
