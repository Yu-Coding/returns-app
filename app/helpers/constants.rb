module Constants

    TOG_REFUND                  = 'TOG_REFUND'
    TOG_EXCHANGE                = 'TOG_EXCHANGE'
    STANDARD_REFUND             = 'STANDARD_REFUND'
    STANDARD_EXCHANGE           = 'STANDARD_EXCHANGE'

    TOG_STORE_REFUND            = 'TOG_STORE_REFUND'
    TOG_ORIGINAL_REFUND         = 'TOG_ORIGINAL_REFUND'
    STANDARD_STORE_REFUND       = 'STANDARD_STORE_REFUND'
    STANDARD_ORIGINAL_REFUND    = 'STANDARD_ORIGINAL_REFUND'

    STORE_METHOD                = 'STORE_METHOD'
    ORIGINAL_METHOD             = 'ORIGINAL_METHOD'

    UNDERWEAR_PRODUCT_TYPE      = 'Underwear'
    SOCKS_PRODUCT_TYPE          = 'Socks'
    OTHER_PRODUCT_TYPE          = 'Other'

    TOG_FABRICS = [
        "18-Hour Jersey",
        "18-Hour Mesh",
        "AIRKNITx",
        "Silver",
        "Woven",
    ]

    FABRIC_UNDERWEAR_OPTIONS = [
      {
        label: '18-Hour Jersey',
        value: 'TOG-18-Hour Jersey'
      },
      {
        label: '18-Hour Mesh',
        value: 'TOG-18-Hour Mesh'
      },
      {
        label: 'AIRKNITx',
        value: 'TOG-AIRKNITx'
      },
      {
        label: 'Silver',
        value: 'TOG-Silver'
      },
      {
        label: 'Woven',
        value: 'TOG-Woven'
      }
    ]

    FABRIC_SOCK_OPTIONS = [
      {
        label: 'One Size',
        value: 'TOG-One Size Sock'
      },
      {
        label: 'Multi Size',
        value: 'TOG-Multi Size Sock'
      }
    ]

    TOG_UNDERWEAR_TAGS = [
        "TOG-18-Hour Jersey",
        "TOG-18-Hour Mesh",
        "TOG-AIRKNITx",
        "TOG-Silver",
        "TOG-Woven",
    ]

    TOG_SOCKS_TAGS = [
        "TOG-One Size Sock",
        "TOG-Multi Size Sock"
    ]

    TOG_Eligible_TAGS = [
        "TOG-18-Hour Jersey",
        "TOG-18-Hour Mesh",
        "TOG-AIRKNITx",
        "TOG-Silver",
        "TOG-Woven",
        "TOG-One Size Sock",
        "TOG-Multi Size Sock"
    ]

    UNDERWEAR_STYLES = [
        "Boxer Briefs",
        "Extended Boxer Briefs",
        "Briefs",
        "Boxers",
        "Long Underwear",
        "Trunks",
    ]

    SOCKS_STYLES = [
        "Low",
        "High",
    ]

    TAG_18_HOUR_JERSEY          = "TOG-18-Hour Jersey"
    TAG_18_HOUR_MESH            = "TOG-18-Hour Mesh"
    TAG_AIRKNITx                = "TOG-AIRKNITx"
    TAG_SILVER                  = "TOG-Silver"
    TAG_WOVEN                   = "TOG-Woven"
    TAG_ONE_SIZE_SOCK           = "TOG-One Size Sock"
    TAG_MULTI_SIZE_SOCK         = "TOG-Multi Size Sock"

end