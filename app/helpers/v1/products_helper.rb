module V1::ProductsHelper
    include Constants

    # get product by id
    def getProductById(product_id)
        begin
            product = ShopifyAPI::Product.find(product_id)
            product
        rescue ActiveResource::ResourceNotFound => e
            nil
        end
    end

    def get_product_title(product_id)
        begin
            product = ShopifyAPI::Product.find(product_id)
            product.title
        rescue ActiveResource::ResourceNotFound => e
            nil
        end
    end

    # get collection id
    def getCollectionId tag
        # QA Store
        # case tag
        # when Constants::TAG_18_HOUR_JERSEY
        #     collection_id = '135958495304'
        # when Constants::TAG_18_HOUR_MESH
        #     collection_id = '135973699656'
        # when Constants::TAG_AIRKNITx
        #     collection_id = '135973765192'
        # when Constants::TAG_SILVER
        #     collection_id = '135973797960'
        # when Constants::TAG_WOVEN
        #     collection_id = '135973896264'
        # when Constants::TAG_ONE_SIZE_SOCK
        #     collection_id = '135973929032'
        # when Constants::TAG_MULTI_SIZE_SOCK
        #     collection_id = '135974027336'
        # end

        # live store
        case tag
        when Constants::TAG_18_HOUR_JERSEY
            collection_id = '136911552625'
        when Constants::TAG_18_HOUR_MESH
            collection_id = '136096153713'
        when Constants::TAG_AIRKNITx
            collection_id = '136911585393'
        when Constants::TAG_SILVER
            collection_id = '136911618161'
        when Constants::TAG_WOVEN
            collection_id = '136096284785'
        when Constants::TAG_ONE_SIZE_SOCK
            collection_id = '135973929032'
        when Constants::TAG_MULTI_SIZE_SOCK
            collection_id = '135974027336'
        end
        collection_id
    end

    # get TOG eligible options(variants, images, size, color)
    def getTogEligibleOptions(tag, price)
        # get collection_id from tag
        collection_id = getCollectionId tag
        # get all product ids for fabric
        products = ShopifyAPI::Product.find(:all, :params => {
            collection_id: collection_id,
            # fields: 'id'
        })

        # images, colors, sizes, variants = Array.new(4) { [] }
        images = {}
        colors = {}
        sizes  = {}
        variants = {}

        products.each do |product|
            sub_variants = product.variants.select { |variant| variant.price == price }
            sub_images = product.images
            sub_sizes = product.variants.map { |variant| variant.option2 }
            sub_sizes = sub_sizes.uniq
            sub_colors = product.variants.map { |variant| variant.option1 }
            sub_colors = sub_colors.uniq

            product_type = product.product_type
            tags = product.tags
            if product_type == "Underwear"
                if tags.include? "Boxer Briefs"
                    if variants['Boxer-Briefs'].nil?
                        variants['Boxer-Briefs'] = sub_variants
                    else
                        variants['Boxer-Briefs'] = variants['Boxer-Briefs'] | sub_variants
                    end
                    if images['Boxer-Briefs'].nil?
                        images['Boxer-Briefs'] = sub_images
                    else
                        images['Boxer-Briefs'] = images['Boxer-Briefs'] | sub_images
                    end
                    if sizes['Boxer-Briefs'].nil?
                        sizes['Boxer-Briefs'] = sub_sizes
                    else
                        sizes['Boxer-Briefs'] = sizes['Boxer-Briefs'] | sub_sizes
                    end
                    if colors['Boxer-Briefs'].nil?
                        colors['Boxer-Briefs'] = sub_colors
                    else
                        colors['Boxer-Briefs'] = colors['Boxer-Briefs'] | sub_colors
                    end
                elsif tags.include? "Extended Boxer Briefs"
                    if variants['Extended-Boxer-Briefs'].nil?
                        variants['Extended-Boxer-Briefs'] = sub_variants
                    else
                        variants['Extended-Boxer-Briefs'] = variants['Extended-Boxer-Briefs'] | sub_variants
                    end
                    if images['Extended-Boxer-Briefs'].nil?
                        images['Extended-Boxer-Briefs'] = sub_images
                    else
                        images['Extended-Boxer-Briefs'] = images['Extended-Boxer-Briefs'] | sub_images
                    end
                    if sizes['Extended-Boxer-Briefs'].nil?
                        sizes['Extended-Boxer-Briefs'] = sub_sizes
                    else
                        sizes['Extended-Boxer-Briefs'] = sizes['Extended-Boxer-Briefs'] | sub_sizes
                    end
                    if colors['Extended-Boxer-Briefs'].nil?
                        colors['Extended-Boxer-Briefs'] = sub_colors
                    else
                        colors['Extended-Boxer-Briefs'] = colors['Extended-Boxer-Briefs'] | sub_colors
                    end
                elsif tags.include? "Briefs"
                    if variants['Briefs'].nil?
                        variants['Briefs'] = sub_variants
                    else
                        variants['Briefs'] = variants['Briefs'] | sub_variants
                    end
                    if images['Briefs'].nil?
                        images['Briefs'] = sub_images
                    else
                        images['Briefs'] = images['Briefs'] | sub_images
                    end
                    if sizes['Briefs'].nil?
                        sizes['Briefs'] = sub_sizes
                    else
                        sizes['Briefs'] = sizes['Briefs'] | sub_sizes
                    end
                    if colors['Briefs'].nil?
                        colors['Briefs'] = sub_colors
                    else
                        colors['Briefs'] = colors['Briefs'] | sub_colors
                    end
                elsif tags.include? "Boxers"
                    if variants['Boxers'].nil?
                        variants['Boxers'] = sub_variants
                    else
                        variants['Boxers'] = variants['Boxers'] | sub_variants
                    end
                    if images['Boxers'].nil?
                        images['Briefs'] = sub_images
                    else
                        images['Boxers'] = images['Boxers'] | sub_images
                    end
                    if sizes['Boxers'].nil?
                        sizes['Boxers'] = sub_sizes
                    else
                        sizes['Boxers'] = sizes['Boxers'] | sub_sizes
                    end
                    if colors['Boxers'].nil?
                        colors['Boxers'] = sub_colors
                    else
                        colors['Boxers'] = colors['Boxers'] | sub_colors
                    end
                elsif tags.include? "Long Underwear"
                    if variants['Long-Underwear'].nil?
                        variants['Long-Underwear'] = sub_variants
                    else
                        variants['Long-Underwear'] = variants['Long-Underwear'] | sub_variants
                    end
                    if images['Long-Underwear'].nil?
                        images['Long-Underwear'] = sub_images
                    else
                        images['Long-Underwear'] = images['Long-Underwear'] | sub_images
                    end
                    if sizes['Long-Underwear'].nil?
                        sizes['Long-Underwear'] = sub_sizes
                    else
                        sizes['Long-Underwear'] = sizes['Long-Underwear'] | sub_sizes
                    end
                    if colors['Long-Underwear'].nil?
                        colors['Long-Underwear'] = sub_colors
                    else
                        colors['Long-Underwear'] = colors['Long-Underwear'] | sub_colors
                    end
                elsif tags.include? "Trunks"
                    if variants['Trunks'].nil?
                        variants['Trunks'] = sub_variants
                    else
                        variants['Trunks'] = variants['Trunks'] | sub_variants
                    end
                    if images['Trunks'].nil?
                        images['Trunks'] = sub_images
                    else
                        images['Trunks'] = images['Trunks'] | sub_images
                    end
                    if sizes['Trunks'].nil?
                        sizes['Trunks'] = sub_sizes
                    else
                        sizes['Trunks'] = sizes['Trunks'] | sub_sizes
                    end
                    if colors['Trunks'].nil?
                        colors['Trunks'] = sub_colors
                    else
                        colors['Trunks'] = colors['Trunks'] | sub_colors
                    end
                end
            elsif product_type == "Socks"
                if tags.include? "Low"
                    if variants['Low'].nil?
                        variants['Low'] = sub_variants
                    else
                        variants['Low'] = variants['Low'] | sub_variants
                    end
                    if images['Low'].nil?
                        images['Low'] = sub_images
                    else
                        images['Low'] = images['Low'] | sub_images
                    end
                    if sizes['Low'].nil?
                        sizes['Low'] = sub_sizes
                    else
                        sizes['Low'] = sizes['Low'] | sub_sizes
                    end
                    if colors['Low'].nil?
                        colors['Low'] = sub_colors
                    else
                        colors['Low'] = colors['Low'] | sub_colors
                    end
                elsif tags.include? "High"
                    if variants['High'].nil?
                        variants['High'] = sub_variants
                    else
                        variants['High'] = variants['High'] | sub_variants
                    end
                    if images['High'].nil?
                        images['High'] = sub_images
                    else
                        images['High'] = images['High'] | sub_images
                    end
                    if sizes['High'].nil?
                        sizes['High'] = sub_sizes
                    else
                        sizes['High'] = sizes['High'] | sub_sizes
                    end
                    if colors['High'].nil?
                        colors['High'] = sub_colors
                    else
                        colors['High'] = colors['High'] | sub_colors
                    end
                end
            end
        end

        product_ids = products.map { |product| product.id }
        # check if customer already did TOG Exchange
        reimbursement_type_id = get_reimbursement_type_id(Constants::TOG_EXCHANGE)
        count = Product.where(reimbursement_type_id: reimbursement_type_id, exchange_product_id: product_ids).count
        {
            "tog_eligible" => count == 0,
            "variants" => variants,
            "images" => images,
            "colors" => colors,
            "sizes" => sizes,
        }
    end

    # get tog info of product
    def getTogInfo product, price
        if product.nil?
            {
                tog_eligible: false
            }
        else
            tag = togEligibleTag product

            if tag.blank?
                {
                    tog_eligible: false
                }
            elsif tag == Constants::TAG_ONE_SIZE_SOCK
                options = getTogEligibleOptions(tag, price)
                {
                    tog_eligible: options["tog_eligible"],
                    styles: getStyles(product),
                    variants: options["variants"],
                    images: options["images"],
                    colors: options["colors"],
                    sizes: options["sizes"],
                    refund_available: true,
                    exchange_available: false
                }
            elsif tag == Constants::TAG_MULTI_SIZE_SOCK
                options = getTogEligibleOptions(tag, price)
                {
                    tog_eligible: options["tog_eligible"],
                    styles: getStyles(product),
                    variants: options["variants"],
                    images: options["images"],
                    colors: options["colors"],
                    sizes: options["sizes"],
                    refund_available: true,
                    exchange_available: true
                }
            else
                options = getTogEligibleOptions(tag, price)
                {
                    tog_eligible: options["tog_eligible"],
                    styles: getStyles(product),
                    variants: options["variants"],
                    images: options["images"],
                    colors: options["colors"],
                    sizes: options["sizes"],
                    refund_available: true,
                    exchange_available: true
                }
            end
        end
    end

    # get styles from product
    def getStyles product
        product_type = product.product_type
        if product_type == Constants::UNDERWEAR_PRODUCT_TYPE
            Constants::UNDERWEAR_STYLES
        elsif product_type == Constants::SOCKS_PRODUCT_TYPE
            Constants::SOCKS_STYLES
        end
    end

    # check if product is TOG eligible
    def togEligibleTag product
        tag = ""
        tags = product.tags.split(',')

        puts product.id

        tags.each do |t|
            t = t.strip
            if Constants::TOG_Eligible_TAGS.include? t
                tag = t
            end
        end
        tag
    end

end
