module V1::VariantsHelper
    include BaseHelper

    # get variant by id
    def get_variant variant_id
        begin
            variant = ShopifyAPI::Variant.find(variant_id)
            variant
        rescue ActiveResource::ResourceNotFound => e
            nil
        end
    end

    # get product id from variant id
    def get_product_id_from_variant_id variant_id
        variant = get_variant variant_id
        variant.nil? ? nil : variant.product_id
    end

    # get price of product with variant
    def get_price(variant_id)
        variant = get_variant(variant_id)
        variant.present? ? variant.price : 0
    end

    # get total price of products using variant_ids
    def getTotalPrice(variant_ids)
        totalPrice = 0
        variant_ids.each { |id| totalPrice+= get_price(id) }
        totalPrice
    end

    # get product image
    def getImage product_id, variant_id
        begin
            variant = ShopifyAPI::Variant.find(variant_id)
            image_id = variant.image_id
            if image_id.nil?
                images = ShopifyAPI::Image.find(:all, :params => {:product_id => product_id})
                images.present? ? images.first.src : nil
            else
                image = ShopifyAPI::Image.find(image_id, :params => {:product_id => product_id})
                image.src
            end
        rescue ActiveResource::ResourceNotFound => e
            nil
        end
    end

    # get return info
    def getReturnInfo order_id, variant_id
        if order_id.nil? or variant_id.nil?
            {
                returned: false,
                selected: false,
                return_id: nil
            }
        else
            # check if the product is already return/exchange
            products = Product.where(order_id:order_id, variant_id: variant_id)
            count = products.count
            if count > 0
                info = products.first
                reason = ReturnReason.find(info.return_reason_id).reason
                if info.return_method_id.nil?
                    method = nil
                else
                    method = ReturnMethod.find(info.return_method_id).description
                end
                {
                    detail: {
                        order_id: info.order_id,
                        reason: reason,
                        method: method,
                        created_at: info.created_at,
                        updated_at: info.updated_at,
                    },
                    returned: true,
                    selected: false,
                    return_id: info.return_id,
                    processed: info.processed
                }
            else
                {
                    returned: false,
                    selected: false,
                    return_id: nil,
                    processed: false
                }
            end
        end
    end
end
