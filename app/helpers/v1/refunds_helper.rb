module V1::RefundsHelper
    include Constants
    include V1::OrdersHelper

    def calculate_refund_old(order_id, line_items)
        refund = ShopifyAPI::Refund.calculate(
            {
                :shipping => { :full_refund => true },
                :refund_line_items => line_items
            },
            :params => {
                :order_id => order_id
            }
        )
        refund
    end

    # calculate refund
    def calculate_refund(original_order, refund_line_items)
        # amount = original_order.total_tax.to_f + original_order.total_shipping_price_set.shop_money.amount.to_f
        # puts 'amount = '
        # puts amount
        refund = ShopifyAPI::Refund.calculate(
            {
                :currency => "USD",
                :shipping => {
                    :full_refund => true
                    # :amount => amount.to_s
                },
                :refund_line_items => refund_line_items,
            },
            :params => {
                :order_id => original_order.id
            }
        )
        refund
    end

    # make refund line items
    def make_refund_line_items(original_order, line_items)
        refund_line_items = line_items.map do |item|
            {
                :line_item_id => item.id,
                :quantity => item.quantity,
                :restock_type => 'no_restock',
                :location_id => original_order.location_id,
            }
        end
        refund_line_items
    end

    # update transactions
    def update_transactions_for_refund(transactions)
        updated_transactions = transactions.map do |transaction|
            {
                :parent_id => transaction.parent_id,
                :amount => transaction.amount,
                :kind => 'refund',
                :gateway => transaction.gateway
            }
        end
        updated_transactions
    end

    # create refund(old)
    def create_refund_old(type, payment_method, order_id, line_items)
        # order = ShopifyAPI::Order.find(:all, params: {
        #     ids: '1077647999048'
        #     status: 'canceled'
        # }).first
        # order.destroy

        # order = get_order(order_id)
        # # make refund_line_items
        # refund_line_items = line_items.map do |item|
        #     {
        #         :line_item_id => item.id,
        #         :quantity => item.quantity,
        #         :restock_type => 'no_restock',
        #         :location_id => order.location_id
        #     }
        # end

        # # calculated the refund for line items
        # refund_calculated = calculate_refund_old(order_id, refund_line_items)
        # # make transactions
        # transactions_for_refund = refund_calculated.transactions.map do |t|
        #     {
        #         "parent_id": t.parent_id,
        #         "amount": t.amount,
        #         "kind": t.kind,
        #         "gateway": t.gateway
        #     }
        # end

        # # create a refund
        # refund = ShopifyAPI::Refund.new(
        #     :currency => 'USD',
        #     :order_id => order_id,
        #     :notify => true,
        #     :note => 'refund test', # ask
        #     :shipping => { :full_refund => true }, # ask
        #     :refund_line_items => refund_line_items,
        #     # :transactions => refund_calculated.transactions
        #     # :transactions => [
        #     #     {
        #     #         "parent_id": 801038806,
        #     #         "amount": ,
        #     #         "kind": "refund",
        #     #         "gateway": "shopify_payments"
        #     #     }
        #     # ]
        #     # :transactions => transactions_for_refund
        # )
        # refund.save
        # refund
        # mark order as refunded
        mark_order_as_refunded_old(type, order_id)
    end

    # create refund
    def create_refund(original_order, original_variant_ids, reason)
        line_items = get_line_items(original_order, original_variant_ids)

        refund_line_items = make_refund_line_items(original_order, line_items)

        c_refund = calculate_refund(original_order, refund_line_items)

        puts '============================='
        puts 'c_refund = '
        puts c_refund.to_json

        refund = ShopifyAPI::Refund.create(
            # :currency => 'USD',
            :order_id => original_order.id,
            :notify => true,
            :note => reason,
            :shipping => { :full_refund => true },
            :refund_line_items => refund_line_items,
            :transactions => update_transactions_for_refund(c_refund.transactions)
        )

        puts 'refund = '
        puts refund.to_json
        puts '============================='
    end

    def refund_calculation line_items
      ShopifyAPI::Refund.calculate({
        :currency => "USD",
        :shipping => {
            :full_refund => true
        },
        :refund_line_items => line_items.map { |e| {
            :line_item_id => e.id,
            :quantity => e.quantity,
            :restock_type => 'no_restock',
            :location_id => @order.location_id,
          }},
        },
        :params => {
        :order_id => @order.id
      })
    end

end