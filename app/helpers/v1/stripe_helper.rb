module V1::StripeHelper
    include V1::OrdersHelper
    # before_action :set_token

    def set_token
        Stripe.api_key = ENV['STRIPE_SECRET']
        # Stripe.api_key = 'sk_test_WDv0ZCRYJEsVRPbzOXorbG4s00pNT70iCU'
    end

    # auto charge using stripe
    def stripeCharge(order_id, stripe_customer_id)
        set_token

        # get shipping
        order = get_order(order_id)
        if order.present?
            total_price = getTotalAmountForAutoCharge(order_id)
            if total_price <= 0
                # get toal amount for auto charge since customer didn't send items
                shipping_price = order.present? ? order.shipping_lines.price : 0
                amount = total_price + shipping_price

                puts "auto charging start..."
                puts amount

                # charge = Stripe::Charge.create({
                #     amount: amount,
                #     currency: 'usd',
                #     # source: token,
                #     customer: stripe_customer_id,
                #     description: "You didn't send items in 30 days. Order id is",
                #     receipt_email: 'test@test.com',
                #     # shipping: {
                #     #   name: customer.name,
                #     #   address: {
                #     #     line1: address.address1,
                #     #     line2: address.address2,
                #     #     city: address.city,
                #     #     state: address.state,
                #     #     postal_code: address.postal_code,
                #     #     country: address.country
                #     #   }
                #     # }
                # })
            end
        end
    end

    # check if needs auto charge and do auto charge
    def checkAutoCharge
        reimbursement_type_id = get_reimbursement_type_id(Constants::STANDARD_EXCHANGE)
        rows = Customer.group(:order_id, :id)

        rows.each do |row|
            stripeCharge(row.order_id, row.stripe_id)
        end
    end

    # save customer info
    def save_customer_info(customer_id, stripe_customer_id, order_id, customer_data)
        customer_data[:customer_id] = customer_id
        customer_data[:stripe_id] = stripe_customer_id
        customer_data[:order_id] = order_id

        Customer.create(customer_data)
    end

    # create stripe customer
    def create_stripe_customer(token, metadata)
        set_token
        # save user's credit card info
        customer_data = {
            source: token,
            email: (metadata[:email] if metadata[:email].present?),
            meatdata: (metadata[:meta] if metadata[:meta].present?),
            address: (metadata[:address] if metadata[:address].present?),
        }.compact
        customer = Stripe::Customer.create(customer_data)
        customer
    end
end
