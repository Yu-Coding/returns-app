require 'date'
module V1::OrdersHelper
    include BaseHelper
    include V1::VariantsHelper

    # get order by order id
    def get_order(order_id)
        ShopifyAPI::Order.find(order_id)
    end

    # get new order id for exchange
    def get_exchange_order_id(order_id, reimbursement_type_id)
        Order.where(order_id: order_id, reimbursement_type_id: reimbursement_type_id).first.new_order_id
    end

    def getDetailedOrders(options)
        orders = ShopifyAPI::Order.find(:all, params: options)
        puts orders.length
        orders.each do |order|
            line_items = order.line_items
            line_items.each do |item|
                item.extra = {}
                begin
                    product = ShopifyAPI::Product.find(item.product_id,
                        # params: {
                        #     fields: 'image,images,variants,options,tags,product_type'
                        # }
                    )
                    image = product.images.find{|image| image.variant_ids.include?(item.variant_id)}
                    item.extra = {
                        'images': product.images,
                        'image': image.present? ? image.src : product.image.present? ? product.image.src : 'https://cdn.shopify.com/s/images/admin/no-image-compact.gif',
                        'variants': product.variants.select { |variant| variant.price == item.price },
                        'options': product.options,
                        'tags': product.tags,
                        'product_type': product.product_type,
                        'handle': product.handle,
                        'tog_info': getTogInfo(product, item.price),
                        'return_info': getReturnInfo(order.id, item.variant_id)
                    }
                rescue ActiveResource::ResourceNotFound => e
                    item.extra = {
                        'images': [],
                        'image': nil,
                        'variants': [],
                        'options': [],
                        'tags': nil,
                        'product_type': nil,
                        'handle': nil,
                        'tog_info': getTogInfo(nil, nil),
                        'return_info': getReturnInfo(nil, nil)
                    }
                end

                begin
                    variant = ShopifyAPI::Variant.find(item.variant_id, params: {
                        fields: 'option1,option2'
                    })
                    item.extra['color'] = variant.option1
                    item.extra['size'] = variant.option2
                rescue ActiveResource::ResourceNotFound => e
                end
            end
        end
    end

    def getOrderOptions(page, limit)
        options = {
            status: 'archived',
            limit: limit,
            page: page,
            order: "created_at DESC",
        }
        options
    end

    def getOrdersOptionsByRange(range, options)
        @today = Date.today

        # last 6/12 months orders
        if range.include? 'last'
            month = range.sub('last-', '').to_i
            created_at_min = month.month.ago.beginning_of_day
            options.store('created_at_min', created_at_min)
        # [year] and before orders
        elsif range.include? 'before'
            year = range.sub('before-', '').to_i
            created_at_max = @today.change(year: year).end_of_year.strftime("%FT%T")
            options.store('created_at_max', created_at_max)
        # orders by year
        else
            year = range.to_i
            created_at_min = @today.change(year: year).beginning_of_year.strftime("%FT%T")
            created_at_max = @today.change(year: year).end_of_year.strftime("%FT%T")
            options.store('created_at_min', created_at_min)
            options.store('created_at_max', created_at_max)
        end
        options
    end

    # get line item ids from order & variant_ids
    def get_line_items(order, variant_ids)
        line_items = order.line_items.select{ |item| variant_ids.include? item.variant_id }.map{ |item| item }
        line_items
    end

    # make line items with exchange variant ids
    def make_line_items(line_items, original_variant_ids, exchange_variant_ids)
        line_items_for_order = line_items.map do |item|
            {
                :quantity => item.quantity,
                :variant_id => exchange_variant_ids[original_variant_ids.index(item.variant_id)]
            }
        end
        line_items_for_order
    end

    # get all line item ids of order
    def get_all_line_item_ids(order)
        line_item_ids = order.line_items.map { |item| item.id }

        line_item_ids
    end

    # get all variant ids of order
    def get_all_variant_ids(order_id)
        order = get_order(order_id)
        variant_ids = order.line_items.map { |item| item.variant_id }

        variant_ids
    end

    # get refuned line item ids
    def get_refunded_line_item_ids(order)
        line_item_ids = get_all_line_item_ids(order)

        refunds = ShopifyAPI::Refund.find(:all, :params => {:order_id => order.id})
        refunded_line_item_ids = refunds.map do |refund|
            refund.refund_line_items.map { |item| item.line_item_id }
        end
        refunded_line_item_ids = refunded_line_item_ids.flatten

    end

    # check if this item is last refund items


    # get all variant ids of order for not TOG eligible product(variant)
    def getAllNotTOGVariantIds(order_id)
        variant_ids = Array.new

        order = get_order(order_id)
        order.line_items do |item|
            if !checkTOGProduct(item.variant_id)
                variant_ids.push item.variant_id
            end
        end

        variant_ids
    end

    # get customer of order
    def get_customer(order)
        order.key?('customer') ? order.customer : nil
    end

    # get customer id of order
    def get_customer_id(order)
        order.key?('customer') ? order.customer.id : nil
    end

    # get total amount of not received items of specific order
    def getTotalAmountForAutoCharge(order_id)
        reimbursement_type_id = get_reimbursement_type_id(Constants::STANDARD_EXCHANGE)
        # get variant ids for not received items in 60 days
        variant_ids = Product.where(order_id: order_id, reimbursement_type_id: reimbursement_type_id, received: 0)
                            .where("updated_at < ?", 60.days.ago)
                            .pluck(:variant_id)
        getTotalPrice(variant_ids)
    end

    # create a order on DB
    def save_order_info(original_order_id, new_order_id, reimbursement_type_id, status)
        order = Order.create(
            order_id: original_order_id,
            new_order_id: new_order_id,
            reimbursement_type_id: reimbursement_type_id,
            status: status
        )
        order
    end

    # create a shopify order
    def create_shopify_order(original_order, original_variant_ids, exchange_variant_ids, financial_status, tag)
        # get line items
        line_items = get_line_items(original_order, original_variant_ids)

        # make line items for order
        line_items_for_order = []
        total_line_items_price = 0
        line_items.each do |item|
            exchange_variant_id = exchange_variant_ids[original_variant_ids.index(item.variant_id)]
            line_items_for_order <<
                {
                    :quantity => item.quantity,
                    :variant_id => exchange_variant_id
                }
            total_line_items_price += get_price(exchange_variant_id).to_f * item.quantity
        end

        amount = total_line_items_price + original_order.total_tax.to_f + original_order.total_shipping_price_set.shop_money.amount.to_f

        # create a pending order
        if financial_status == 'pending'
            order = ShopifyAPI::Order.create(
                :financial_status => 'pending',
                :customer => {
                    :id => original_order.customer.id
                    # :id => '1846377185352'
                },
                :transactions => [
                    {
                        :amount => amount,
                        :kind => "authorization",
                        :status => "success"
                    }
                ],
                :total_shipping_price_set => original_order.total_shipping_price_set,
                :total_tax => original_order.total_tax,
                :line_items => line_items_for_order,
                :tags => tag
            )
        elsif financial_status == 'paid'
            order = ShopifyAPI::Order.create(
                :financial_status => 'pending',
                :customer => {
                    :id => original_order.customer.id
                    # :id => '1846377185352'
                },
                :transactions => [
                    {
                        :amount => amount,
                        :kind => "authorization",
                        :status => "success"
                    }
                ],
                :total_shipping_price_set => original_order.total_shipping_price_set,
                :total_tax => original_order.total_tax,
                :line_items => line_items_for_order,
                :tags => tag
            )

            mark_order_as_paid(order.id, nil)
        end
        order
    end

    # check order if order is refunded
    def checkOrderRefunded(type, order_id)
        # if type == Constants::TOG_REFUND || type == Constants::STANDARD_REFUND
            line_item_ids = get_all_line_item_ids(order)

            refunds = ShopifyAPI::Refund.find(:all, :params => {:order_id => order_id})
            refunded_line_item_ids = refunds.map do |refund|
                refund.refund_line_items.map { |item| item.line_item_id }
            end
            refunded_line_item_ids = refunded_line_item_ids.flatten

            line_item_ids.sort == refunded_line_item_ids.sort
        # elsif type == Constants::STANDARD_EXCHANGE
        #     type_id = get_reimbursement_type_id(Constants::STANDARD_EXCHANGE)
        #     variant_ids = get_all_variant_ids(order)
        #     # get all received variants
        #     received_variant_ids = Product.where(order_id: order_id, reimbursement_type_id: type_id, received: 1).pluck(:variant_id)
        #     variant_ids.sort == received_variant_ids.sort
        # end
    end

    # mark order as refunded/partially refunded
    def mark_order_as_refunded(original_order, original_variant_ids)
        if original_order.line_items.length == exchange_variant_ids.length
            ShopifyAPI::Transaction.create({:order_id => original_order.id, :kind => 'refund'})
        else
            line_items = get_line_items(original_order, original_variant_ids)

            refund_line_items = make_refund_line_items(original_order, line_items)

            refund = calculate_refund(original_order, refund_line_items)

            ShopifyAPI::Transaction.create({
                :order_id => original_order.id,
                :kind => 'refund',
                :amount => refund.transactions.first.amount
            })
        end
    end

    # mark order as refunded/partially refunded(old)
    def mark_order_as_refunded_old(type, order_id)
        # check if order is refunded or partially refunded
        # financial_status = checkOrderRefunded(type, order_id) ? 'refunded' : 'partially_refunded'
        ShopifyAPI::Transaction.create({:order_id => order_id, :kind => 'refund'})
    end

    # create an transaction for capturing order - paid/partially paid
    def mark_order_as_paid(order_id, amount)
        # get transactions
        transaction = ShopifyAPI::Transaction.find(:all, :params => {:order_id => order_id}).first

        transaction_option = {
            :order_id => order_id,
            :kind => 'capture',
            :parent_id => transaction.id,
            :status => "success",
        }

        if amount.present?
            # partially paid
            transaction_option[:amount] = amount
        end
        ShopifyAPI::Transaction.create(transaction_option)
    end

    # mark order as paid/pending
    def mark_order_as_paid_old(type, order_id)
        order = get_order(order_id)

        if type == Constants::TOG_EXCHANGE
            order.financial_status = 'paid'
        elsif type == Constants::STANDARD_EXCHANGE
            # check if all items which user sent for exchange are received
            need_send_item_count = Product.where(order_id: order_id, reimbursement_type_id: type_id ).count
            received_count = Product.where(order_id: order_id, reimbursement_type_id: type_id, received: 1).count

            if need_send_item_count == received_count
                order.financial_status = 'paid'
            elsif received_count.present?
                order.financial_status = 'partially_paid'
            else
                order.financial_status = 'pending'
            end
        end
        order.save
    end

    # mark tags to order
    def markTagsToOrder(order, id)
        order.tags = "#{order.tags.present? ? ',' : ''}R-#{id}"
        order.save
    end

    def create_db_order_record(original_order_id, new_order_id, status)
      order = Order.new({
        order_id: original_order_id,
        new_order_id: new_order_id,
        status: status
      })
      unless order.save
        puts order.errors.full_messages
      end
    end

    def create_gift_card_to_customer amount
      return false if @order.customer.nil?
      gift_card = ShopifyAPI::GiftCard.new({
        initial_value: amount,
        customer_id: @order.customer.id,
        note: 'Gift card issued with refund as store credit.'
      })
      if gift_card.save
        puts gift_card.to_json
      else
        puts gift_card.errors.full_messages
      end
    end
end
