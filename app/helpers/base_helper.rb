require 'nokogiri'
require 'open-uri'
require 'css_parser'
include CssParser

module BaseHelper
    include Constants

    # def connect_shopify_session shop_id
    #     @shop = Shop.find(shop_id)
    #     @shop.connect
    # end

    def get_reimbursement_type(normal_type, return_type)
        if normal_type == 'TOG' && return_type == 'refund'
            Constants::TOG_REFUND
        elsif normal_type == 'TOG' && return_type == 'exchange'
            Constants::TOG_EXCHANGE
        elsif normal_type == 'STANDARD' && return_type == 'refund'
            Constants::STANDARD_REFUND
        elsif normal_type == 'STANDARD' && return_type == 'exchange'
            Constants::STANDARD_EXCHANGE
        end
    end

    def get_reimbursement_type_id(type)
        if type == Constants::TOG_REFUND
            ReimbursementType.where(normal_type: 'TOG', return_type: 'refund').first.id
        elsif type == Constants::TOG_EXCHANGE
            ReimbursementType.where(normal_type: 'TOG', return_type: 'exchange').first.id
        elsif type == Constants::STANDARD_REFUND
            ReimbursementType.where(normal_type: 'STANDARD', return_type: 'refund').first.id
        elsif type == Constants::STANDARD_EXCHANGE
            ReimbursementType.where(normal_type: 'STANDARD', return_type: 'exchange').first.id
        end
    end

    def get_payment_method(method)
        description = method.description
        status = ReimbursementType.find(method.reimbursement_type_id)
        if status == Constants::TOG_REFUND 
            if description.include? 'store'
                Constants::TOG_STORE_REFUND
            elsif description.include? 'original'
                Constants::TOG_ORIGINAL_REFUND
            end
        elsif status == Constants::STANDARD_REFUND
            if description.include? 'store'
                Constants::STANDARD_STORE_REFUND
            elsif description.include? 'original'
                Constants::STANDARD_ORIGINAL_REFUND
            end
        end
    end

    def curl(url, colors)
        page = Nokogiri::HTML(open(url))
        contents = page.css("div#shopify-section-colors style")
        
        styles = {}
        
        if colors.present? && contents.present?
            style_content = contents[0].text
            colors.each do |color|
                attribute = color_attribute color

                parser = CssParser::Parser.new
                parser.load_string! style_content
                # style = parser.find_by_selector(attribute)
                
                hash = parser.to_h
                if hash['all'].key?(attribute)
                    key = hash['all'][attribute].keys.first.to_s
                    value = hash['all'][attribute].values.first.to_s

                    style = {}
                    style[key] = value
                    styles[color] = style
                end
            end
        end
        styles
    end

    def color_attribute color
        color = color.downcase
        color = color.strip
        color = color.tr('/', '')
        color = color.gsub(/\s+/, " ")
        color = color.tr(" ", '-')
        attribute = '[data-handle-name="' + color + '"]'
        attribute
    end

    def smart_add_url_protocol url
        unless url[/\Ahttp:\/\//] || url[/\Ahttps:\/\//]
            url = "https:#{url}"
            url
        end
    end

    def formatted_number number
        '%09d' % number
    end

    def items_text items
        items.length.to_s + ( (items.length > 1) ? ' items' : ' item' )
    end
end
