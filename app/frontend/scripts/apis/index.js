import request from '../utils/request'
import {
    getShopDomain
} from '../utils/helper'

/**
 * get orders by date range
 *
 * @param {*} data
 */
export function getOrdersByDateRange (data) {
    let shop = getShopDomain()
    return request({
        url: `/orders?range=${data.range}&customer_id=${data.customer_id}&page=${data.page}&shop=${shop}`,
        method: 'get',
    })
}

/**
 *
 * @param {*} data
 */
export function orderById (id) {
    let shop = getShopDomain()
    return request({
        url: `/order/${id}?shop=${shop}`,
        method: 'get',
    })
}

/**
 *
 */
export function getReturnReasons () {
    return request({
        url: '/reasons',
        method: 'get'
    })
}

/**
 *
 */
export function getReturnContainers () {
    return request({
        url: '/containers',
        method: 'get'
    })
}

/**
 *
 * @param {*} data
 */
export function getReturnMethods (data) {
    return request({
        url: `/methods?tog=${data.normal_type}&type=${data.return_type}`,
        method: 'get'
    })
}

/**
 * get available colors for swatches - STANDARD
 *
 * @param {*} data
 */
export function getAvailableColors (data)
{
    let shop = getShopDomain()
    return request({
        url: `/colors?shop=${shop}`,
        method: 'post',
        data: data
    })
}

/**
 *
 * @param {*} data
 */
export function completeReturn (data) {
    let shop = getShopDomain()
    return request({
        url: `/complete-return?shop=${shop}`,
        data: data,
        method: 'post'
    })
}

export function getExchangeData (params) {
    let shop = getShopDomain()
    return request({
        url: `/exchange-data?shop=${shop}`,
        params: params,
        method: 'get'
    })
}

export function returnDetail (return_id) {
    let shop = getShopDomain()
    return request({
        url: `/return-data?shop=${shop}&return_id=${return_id}`,
        method: 'get'
    })
}