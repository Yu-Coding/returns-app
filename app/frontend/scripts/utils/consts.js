var consts = {
  DESCRIPTION: {
    RETURN: {
      TOG: [
        ' Since this was your first pair, it’s covered by our Try On Guarantee, which means you don’t have to return it. Feel free to recycle it, donate it, or hang on to it for good luck.'
      ],
      STANDARD: [
        'Place the return item(s) in the original or similar packaging, attach the prepaid shipping label, and mail the package back to us. (If the shipping label doesn’t generate after a few seconds, try refreshing the page, or grab it from your return confirmation email.)',
        'Once our warehouse receives the item, you’ll receive another confirmation email. If you have any questions about your return, please refer to our Shipping & Returns policy.'
      ]
    },
    EXCHANGE: {
      TOG: [
        'Since this was your first pair, it’s covered by our Try On Guarantee, which means you don’t have to return it. Feel free to recycle it, donate it, or hang on to it for good luck.',
        'Our replacements are expedited, so we’ll send your new item(s) out right away. It should arrive within 3-5 business days.'
      ],
      STANDARD: [
        'Place the exchange item(s) in the original or similar packaging, attach the prepaid shipping label, and mail the package back to us. (If the shipping label doesn’t generate after a few seconds, try refreshing the page, or grab it from your return confirmation email.)',
        'Once our warehouse receives the item, you’ll receive another confirmation email. If you have any questions about your exchange, please refer to our Shipping & Returns policy.'
      ]
    }
  }
}

export {consts}