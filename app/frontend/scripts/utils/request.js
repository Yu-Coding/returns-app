import axios from 'axios'
import config from '../config.json'

const request = axios.create({
  baseURL: `${[config.API_HOST, config.API_VERSION].join('/')}`,
  headers: {
    'Accept': 'application-json',
    'Content-type': 'application/json'
  }
})

export default request