import Vue from 'vue'
import moment from 'moment'

Vue.filter('formatDate1', function(value) {
    if (value) {
      return moment(String(value)).format('MM/DD')
    }
})

Vue.filter('formatDate2', function(value) {
    if (value) {
      return moment(String(value)).format('MMM DD, YYYY')
    }
})

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('capitalizeAll', function (value) {
  if (!value) return ''
  return value.toString().toUpperCase()
})

Vue.filter('fixed2', function(value) {
  return parseFloat(Math.round(value * 100) / 100).toFixed(2);
})

Vue.filter('style', function(value) {
  return value.replace(/\-/g,' ')
})