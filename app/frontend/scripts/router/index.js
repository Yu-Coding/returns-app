import Vue from 'vue'
import VueRouter from 'vue-router'
import Orders from '../pages/Orders.vue'
import OrderDetails from '../pages/OrderDetails.vue'
import ReturnsExchanges from '../pages/ReturnsExchanges.vue'
import TogReturnsExchanges from '../pages/TogReturnsExchanges.vue'
import StandardReturnsExchanges from '../pages/StandardReturnsExchanges.vue'
import ExchangeDetails from '../pages/ExchangeDetails.vue'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'hash',
    base: window.location.pathname,
    // base: process.env.BASE_URL,
    // base: 'frontend',
    routes: [
        {
            path: '/',
            name: 'Orders',
            component: Orders
        },
        {
            path: '/order/:order_id',
            name: 'OrderDetails',
            component: OrderDetails,
            props: true
        },
        {
            path: '/returns-exchange/:order_id',
            name: 'ReturnsExchanges',
            component: ReturnsExchanges,
            props: true
        },
        {
            path: '/returns-exchanges/:order_id/tog/:line_item_id',
            name: 'TogReturnsExchanges',
            component: TogReturnsExchanges,
            props: true
        },
        {
            path: '/returns-exchanges/:order_id/standard/:line_item_id',
            name: 'StandardReturnsExchanges',
            component: StandardReturnsExchanges,
            props: true
        },
        {
            path: '/returns-exchanges/exchange-details/:return_id',
            name: 'ExchangeDetails',
            component: ExchangeDetails,
            props: true
        },
        {
            path: '*',
            redirect: '/'
        }
    ]
})