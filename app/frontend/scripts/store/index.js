import { orderById } from '../apis/index.js'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        orders: {},
        order: {},
        product: {},
        return_detail: {},
        has_more: false,
        cur_page: 1,
        max_page: 1,
        count: 0,
    },
    // Mutations
    mutations: {
        set_orders(state, orders) {
          state.orders = orders
        },
        set_order(state, order) {
          state.order = order
        },
        set_product(state, product) {
            state.product = product
        },
        set_return_detail(state, detail) {
            state.return_detail = detail
        },
        set_has_more(state, has_more) {
            state.has_more = has_more
        },
        set_cur_page(state, cur_page) {
            state.cur_page = cur_page
        },
        set_max_page(state, max_page) {
            state.max_page = max_page
        },
        set_count(state, count) {
            state.count = count
        }
    },
    // Actions
    actions: {
        set_orders({ state, commit }, orders) {
            commit('set_orders', orders)
        },
        loadOrder({commit}, id) {
          return new Promise((resolve, reject) => {
            orderById(id).then(response => {
              const data = response.data
              commit('set_order', data.order)
              resolve()
            }).catch(err => {
              reject(err)
            })
          })
        },
        set_order({ state, commit }, order) {
            commit('set_order', order)
        },
        set_product({ state, commit }, product) {
            commit('set_product', product)
        },
        set_return_detail({ state, commit }, detail) {
            commit('set_return_detail', detail)
        },
        set_has_more({ state, commit }, has_more) {
            commit('set_has_more', has_more)
        },
        set_cur_page({ state, commit }, cur_page) {
            commit('set_cur_page', cur_page)
        },
        set_max_page({ state, commit }, max_page) {
            commit('set_max_page', max_page)
        },
        set_count({ state, commit}, count) {
            commit('set_count', count)
        }
    },
    // Getters
    getters: {
        cur_page: state => {
            return state.cur_page != 1 ? state.cur_page : 1
        }
    }
})