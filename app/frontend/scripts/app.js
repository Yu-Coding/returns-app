require('./bootstrap')
import Vue from 'vue'
import Vuex from 'vuex'
import VueMask from 'v-mask'
import App from './app.vue'
import router from './router'
import store from './store'
import './utils/filter.js'

import vueCountryRegionSelect from 'vue-country-region-select'
Vue.use(vueCountryRegionSelect)

Vue.use(Vuex)
Vue.use(VueMask)

new Vue({
    render: h => h(App),
    store,
    router
}).$mount('#returns-app')