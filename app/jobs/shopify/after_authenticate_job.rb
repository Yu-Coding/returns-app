module Shopify
  class AfterAuthenticateJob < ActiveJob::Base
    def perform(shop_domain:)
      shop = Shop.find_by(shopify_domain: shop_domain)

      shop.with_shopify_session do
        @themes = ShopifyAPI::Theme.all
        action = ActionController::Base.new()
        @snippet = action.render_to_string(:template => 'snippets/returns', :layout => false)
        @themes.map do |theme|
          ShopifyAPI::Asset.create(key: 'snippets/returns.liquid', value: @snippet, theme_id: theme.id)
        end
      end
    end
  end
end
