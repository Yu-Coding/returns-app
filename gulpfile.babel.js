const dotenv = require('dotenv')
dotenv.config()

const gulp = require('gulp')
const requireDir = require('require-dir')

requireDir('gulp', {
  recurse: true
})

gulp.task('build', gulp.series(
  'clean',
  // 'icons',
  'scripts',
  'styles',
  // 'assets',
  // 'fonts'
))

gulp.task('release', gulp.series('build'))
